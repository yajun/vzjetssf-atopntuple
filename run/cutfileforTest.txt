###############################
#
#This configuration files is for test ATOP
#
###############################

LibraryNames libTopEventSelectionTools libTopEventReconstructionTools libHbbCalibZbbChannel

ObjectSelectionName top::ObjectLoaderStandardCuts
OutputFormat top::HCZC::EventSaver
OutputEvents SelectedEvents
OutputFilename outputTest.root

PerfStats No

NEvents 10000
FirstEvent 0

TDPPath HbbCalibZbbChannel/cross-section/xs_p4128.data
BTagCDIPath xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-03-11_v1.root

IsAFII False

GRLDir GoodRunsLists data18_13TeV/20190318/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml

PRWActualMu_FS GoodRunsLists/data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root
PRWActualMu_AF GoodRunsLists/data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root
PRWLumiCalcFiles GoodRunsLists/data18_13TeV/20190318/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root

#PRWfiles for mc16e

PRWConfigFiles dev/PileupReweighting/share/DSID364xxx/pileup_mc16e_dsid364375_FS.root dev/PileupReweighting/share/DSID364xxx/pileup_mc16e_dsid364376_FS.root dev/PileupReweighting/share/DSID364xxx/pileup_mc16e_dsid364377_FS.root dev/PileupReweighting/share/DSID364xxx/pileup_mc16e_dsid364378_FS.root dev/PileupReweighting/share/DSID364xxx/pileup_mc16e_dsid364379_FS.root dev/PileupReweighting/share/DSID364xxx/pileup_mc16e_dsid364380_FS.root dev/PileupReweighting/share/DSID364xxx/pileup_mc16e_dsid364700_FS.root dev/PileupReweighting/share/DSID364xxx/pileup_mc16e_dsid364701_FS.root dev/PileupReweighting/share/DSID364xxx/pileup_mc16e_dsid364702_FS.root dev/PileupReweighting/share/DSID364xxx/pileup_mc16e_dsid364703_FS.root dev/PileupReweighting/share/DSID364xxx/pileup_mc16e_dsid364704_FS.root dev/PileupReweighting/share/DSID364xxx/pileup_mc16e_dsid364705_FS.root dev/PileupReweighting/share/DSID364xxx/pileup_mc16e_dsid364706_FS.root dev/PileupReweighting/share/DSID364xxx/pileup_mc16e_dsid364707_FS.root dev/PileupReweighting/share/DSID364xxx/pileup_mc16e_dsid364708_FS.root dev/PileupReweighting/share/DSID364xxx/pileup_mc16e_dsid364709_FS.root dev/PileupReweighting/share/DSID364xxx/pileup_mc16e_dsid364710_FS.root dev/PileupReweighting/share/DSID364xxx/pileup_mc16e_dsid364711_FS.root dev/PileupReweighting/share/DSID364xxx/pileup_mc16e_dsid364712_FS.root dev/PileupReweighting/share/DSID410xxx/pileup_mc16e_dsid410471_FS.root dev/PileupReweighting/share/DSID309xxx/pileup_mc16e_dsid309450_FS.root dev/PileupReweighting/share/DSID345xxx/pileup_mc16e_dsid345342_FS.root dev/PileupReweighting/share/DSID700xxx/pileup_mc16e_dsid700148_FS.root dev/PileupReweighting/share/DSID364xxx/pileup_mc16e_dsid364106_FS.root

#V+jets loose selections
ElectronCollectionName Electrons
ElectronPt 7000
ElectronEta 2.47
ElectronID LooseAndBLayerLH
ElectronIDLoose LooseAndBLayerLH
ElectronIsolation FCLoose
ElectronIsolationLoose FCLoose

#V+jets loose muon selections
MuonCollectionName Muons
MuonPt 7000
MuonEta 2.7
MuonIsolation FCLoose
MuonIsolationLoose FCLoose
MuonQuality Loose

PhotonCollectionName None 

#Tau selection
TauCollectionName None #TauJets
TauEtaRegions	  [1.37,1.52]
TauPt		  20000
TauJetIDWP	  RNNLoose
TauEleBDTWP	  Loose
TauEleBDTWPLoose  Loose
TauEleOLR	  True
TauEleOLRLoose	  True

JetCollectionName AntiKt4EMPFlowJets_BTagging201903
JetUncertainties_NPModel CategoryReduction

LargeJetCollectionName AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets
LargeRJESJMSConfig CombMass
LargeRJetPt 200000
LargeRJetEta 2
LargeJetOverlapRemoval False
LargeJetSubstructure None

LargeRJetUncertainties_NPModel CategoryReduction
#LargeRJESUncertaintyConfig D2Beta1,Tau21WTA,Tau32WTA,Split12,Split23,Qw

TrackJetCollectionName AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903
TrackJetPt 7000
TrackJetEta 2.5

JetGhostTrackDecoName None
LargeJetSubstructure None

DoLoose False
DoTight Both
DoSysts Tight

Systematics None #AllLargeRJets,AllMuons

UseAodMetaData True

TruthCollectionName None
TruthJetCollectionName AntiKt4TruthJets
TruthLargeRJetCollectionName AntiKt10TruthJets
TruthElectronCollectionName TruthElectrons
TruthMuonCollectionName TruthMuons
TruthMETCollectionName MET_Truth
#TruthPhotonCollectionName TruthPhotons

BoostedJetTagging SmoothedWZTagger:SmoothWContained80,SmoothedWZTagger:SmoothZContained80,SmoothedWZTagger:SmoothZContained50,SmoothedWZTagger:SmoothWContained50
#SmoothedWZTagger:SmoothWContained80 SmoothedWZTagger:SmoothZContained50 SmoothedWZTagger:SmoothZContained80 

BTaggingTrackJetWP  DL1rmu:FixedCutBEff_60 DL1rmu:FixedCutBEff_70 DL1rmu:FixedCutBEff_77 DL1rmu:FixedCutBEff_85 DL1r:FixedCutBEff_60 DL1r:FixedCutBEff_70 DL1r:FixedCutBEff_77 DL1r:FixedCutBEff_85 #MV2c10:FixedCutBEff_60 MV2c10:FixedCutBEff_70 MV2c10:FixedCutBEff_77 MV2c10:FixedCutBEff_85 
#MV2rmu:FixedCutBEff_60 MV2rmu:FixedCutBEff_70 MV2rmu:FixedCutBEff_70 MV2rmu:FixedCutBEff_77 MV2rmu:FixedCutBEff_85 MV2r:FixedCutBEff_60 MV2r:FixedCutBEff_70 MV2r:FixedCutBEff_77 MV2r:FixedCutBEff_85 DL1:FixedCutBEff_60 DL1:FixedCutBEff_70 DL1:FixedCutBEff_77 DL1:FixedCutBEff_85 DL1rmu:FixedCutBEff_60 DL1rmu:FixedCutBEff_70 DL1rmu:FixedCutBEff_77 DL1rmu:FixedCutBEff_85 DL1r:FixedCutBEff_60 DL1r:FixedCutBEff_70 DL1r:FixedCutBEff_77 DL1r:FixedCutBEff_85

#######################
# Selections
#######################

SUB BASIC
INITIAL
GRL
GOODCALO
PRIVTX
RECO_LEVEL


############################3
# definition of the data periods
################################3


SUB period_2015
RUN_NUMBER >= 276262
RUN_NUMBER <= 284484

SUB period_2016
RUN_NUMBER >= 296939
RUN_NUMBER <= 311481

SUB period_2017
RUN_NUMBER >= 324320
RUN_NUMBER <= 341649

SUB period_2018
RUN_NUMBER >= 348197

###########################################
# Define jet trigger
######################################

SUB JET_2015
. BASIC
. period_2015
TRIGDEC HLT_j360_a10_lcw_sub_L1J100

SUB JET_2016
. BASIC
. period_2016
TRIGDEC HLT_j420_a10_lcw_L1J100

SUB JET_2017
. BASIC
. period_2017
TRIGDEC HLT_j390_a10t_lcw_jes_30smcINF_L1J100

SUB JET_2018
. BASIC
. period_2018
TRIGDEC HLT_j420_a10t_lcw_jes_35smcINF_L1J100

SUB HLT_JET_ALL
. BASIC
TRIGDEC HLT_j400 HLT_j420 HLT_j225_gsc420_boffperf_split HLT_j380 HLT_j225_gsc380_boffperf_split HLT_j340

SUB JET_2015_2016
. BASIC
TRIGDEC HLT_j360_a10_lcw_sub_L1J100 HLT_j420_a10_lcw_L1J100

SELECTION Analysis
. BASIC
JETCLEAN LooseBad
LJET_N 200000 >= 1
EL_N_OR_MU_N 7000 >=2
SAVE
