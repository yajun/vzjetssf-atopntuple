#!/usr/bin/env python

# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
import TopExamples.grid
import mc_p4128
import data_p4309
#import data_p4129
import argparse
import os
import sys
import subprocess
import fileinput

SubCampaign = "mc16e"
Selections = ["hadronic"] #hadronic, 1L, 2L
CutfileBASE = "cutfileBase.txt"
#Names = ["mc16e-zmumu-sherpa221","mc16e-zee-sherpa221","mc16e-ztautau-sherpa221"]
#Names = ["mc16e-wmunu-sherpa221","mc16e-wenu-sherpa221","mc16e-wtaunu-sherpa221"]
#Names = ["mc16a-vqq-sherpa228","mc16a-zbb-sherpa228"]
#Names = ["mc16e-ttbar-nonallhad","mc16e-wenu-sherpa2210","mc16e-wmunu-sherpa2210","mc16e-XWlv-sherpa221","mc16e-singletop"]
#Names = ["mc16d-zll-sherpa228","mc16d-XZll-sherpa221"]
#Names = ["mc16a-vqq-sherpa228","mc16a-zbb-sherpa228"]
#Names = ["mc16e-vqq-sherpa228","mc16e-zbb-sherpa228","mc16e-ttbar-allhad"]
#Names = ["mc16e-zll-sherpa228","mc16e-XZll-sherpa221"]
#Names = ["mc16e-vqq-sherpa228-lund","mc16e-ttbar-aMcAtNloPy8"]
#Names = ["mc16a-ttbar-allhad"]
Names = ["mc16e-ttbar-herwig"]
#Names = ["mc16e-XZll-sherpa221"]
DoUFO = False
Dosyst = False
samples = TopExamples.grid.Samples(Names)

#mc16e
#sherpa225 vqq: mc16e-zqq, mc16e-wqq
#pythia ttbar: mc16e-ttbar-nonallhad, mc16e-ttbar-allhad
#dijets: mc16e-dijets, mc16e-dijets-FJ, mc16e-dijets-sherpa, mc16e-dijets-sherpa-lund,mc16e-dijets-herwig-dipole, mc16e-dijets-herwig-angular
#mc16e-vqq-sherpa228, mc16e-zbb-sherpa228, mc16e-vqq-sherpa228-lund
#mc16e-wmunu-sherpa221, mc16e-wenu-sherpa221, mc16e-wtaunu-sherpa221
#mc16e-zmumu-sherpa221, mc16e-zee-sherpa221, mc16e-ztautau-sherpa221
#mc16e-wenu-sherpa2210,mc16e-wmunu-sherpa2210

GoodRunListConfigs= "GRLDir GoodRunsLists \n"
if(SubCampaign == "data15" or SubCampaign == "data16" or SubCampaign == "mc16a"):
    GoodRunListConfigs += "GRLFile data15_13TeV/20170619/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml data16_13TeV/20180129/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml"
    GoodRunListConfigs += " \n"
    GoodRunListConfigs += "PRWLumiCalcFiles GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root"
    GoodRunListConfigs += " \n"
elif(SubCampaign == "data17" or SubCampaign == "mc16d"):
    GoodRunListConfigs += "GRLFile data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"
    GoodRunListConfigs += " \n"
    GoodRunListConfigs += "PRWActualMu_FS GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root"
    GoodRunListConfigs += " \n"
    GoodRunListConfigs += "PRWActualMu_AF GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root"
    GoodRunListConfigs += " \n"
    GoodRunListConfigs += "PRWLumiCalcFiles GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root"
    GoodRunListConfigs += " \n"
elif(SubCampaign == "data18" or SubCampaign == "mc16e"):
    GoodRunListConfigs += "GRLFile data18_13TeV/20190318/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"
    GoodRunListConfigs += " \n"
    GoodRunListConfigs += "PRWActualMu_FS GoodRunsLists/data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root"
    GoodRunListConfigs += " \n"
    GoodRunListConfigs += "PRWActualMu_AF GoodRunsLists/data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root"
    GoodRunListConfigs += " \n"
    GoodRunListConfigs += "PRWLumiCalcFiles GoodRunsLists/data18_13TeV/20190318/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root"
    GoodRunListConfigs += " \n"
else:
    print("ERROR: SubCampaign needed")

PRWFileConfigs = "PRWConfigFiles "
if("data" not in SubCampaign):
    for List in samples:
        SublistSamples = List.datasets
        for sample_concatenated in SublistSamples:
            sample=TopExamples.grid.getShortenedConcatenatedSample(sample_concatenated) # in the case of comma-separated samples with same DSIDs and same first tags (it's the same sample)
            scope = sample.split('.')[0]
            if 'mc' not in scope:
                continue
            dsid = sample.split('.')[1]
            p = 0
            DSID = ""
            for i in dsid:
                p += 1
                if(p>3):
                    DSID += "x"
                    continue
                DSID += i            
            PRWFileConfigs += "dev/PileupReweighting/share/DSID"+DSID+"/pileup_"+SubCampaign+"_dsid"+dsid+"_FS.root "
elif(SubCampaign == "data15" or SubCampaign == "data16"):
    PRWFileConfigs += "dev/PileupReweighting/share/DSID700xxx/pileup_mc16a_dsid700042_FS.root "
elif(SubCampaign == "data17"):
    PRWFileConfigs += "dev/PileupReweighting/share/DSID700xxx/pileup_mc16d_dsid700042_FS.root "
elif(SubCampaign == "data18"):
    PRWFileConfigs += "dev/PileupReweighting/share/DSID700xxx/pileup_mc16e_dsid700042_FS.root "
else:
    print("ERROR: check your SubCampaign")
PRWFileConfigs += " \n"

SelectionsConfig = "SELECTION Analysis \n"
SelectionsConfig += ". BASIC \n"

if(SubCampaign == "data15"): 
    SelectionsConfig += ". JET_2015 \n"
if(SubCampaign == "data16"):
    SelectionsConfig += ". JET_2016 \n"
if(SubCampaign == "data17"):
    SelectionsConfig += ". JET_2017 \n"
if(SubCampaign == "data18"):
    SelectionsConfig += ". JET_2018 \n"

TriggerConfig = "UseGlobalLeptonTriggerSF True \n"
TriggerConfig += "GlobalTriggers 2015@e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose,mu20_iloose_L1MU15_OR_mu50 2016@e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0,mu26_ivarmedium_OR_mu50 2017@e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0,mu26_ivarmedium_OR_mu50 2018@e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0,mu26_ivarmedium_OR_mu50 \n"
#Global trigger

Tagger = "BoostedJetTagging SmoothedWZTagger:SmoothWContained80,SmoothedWZTagger:SmoothZContained80,SmoothedWZTagger:SmoothZContained50,SmoothedWZTagger:SmoothWContained50 \n"
if(DoUFO):
    Tagger = "#BOOSTEDJETTAGGING \n"

UFOJetCollection =  "AntiKt10UFOCSSKSoftDropBeta100Zcut10Jets"#AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets
UFOJetMass = "UFOSDMass" #CombMass
TruthSDJet = "AntiKt10TruthSoftDropBeta100Zcut10Jets"
   #LargeRJESJMSConfig CombMass)


for sel in Selections:
    SystConfigs = "Systematics "
    if(not Dosyst or "data" in SubCampaign):
        SystConfigs += "none \n"
    elif(sel == "hadronic" ):
        SystConfigs += "AllLargeRJets \n"
    else:
        SystConfigs += "AllLargeRJets \n"#"AllMuons,AllElectrons,AllMET \n"
    SelectionsConfig += ". Analysis_" + sel
    SelectionsConfig += " \n"
    SelectionsConfig += "SAVE"
    CutFile = CutfileBASE.replace("Base.txt","_"+SubCampaign+"_"+sel+".txt")
    args=['cp',CutfileBASE, CutFile]
    cpOut=subprocess.check_output(args)
    if cpOut:
        print('cp command output:\n',cpOut)
    if(sel == "hadronic"):
        TriggerConfig = "#LEPTONTRIGS"

    replaceDict = {
        "GRUNLISTCONFIGS" : GoodRunListConfigs, 
        "PRWCONFIGFILES" : PRWFileConfigs,
        "SYSTEMATICS" : SystConfigs,
        "BOOSTEDJETTAGGING" : Tagger,
        "LEPTONTRIGS" : TriggerConfig,
        "SELECTIONCONFIGS" : SelectionsConfig
    }
    for key,value in replaceDict.items():
        for line in fileinput.input(CutFile,inplace=True):
            if key in line:
                line = line.replace(key,value)
            sys.stdout.write(line)

    replaceDictUFO = {
        "AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets" : UFOJetCollection,
        "CombMass" : UFOJetMass,
        "AntiKt10TruthTrimmedPtFrac5SmallR20Jets" : TruthSDJet
    }
    if(DoUFO):
        for key,value in replaceDictUFO.items():
            for line in fileinput.input(CutFile,inplace=True):
                if key in line:
                    line = line.replace(key,value)
                sys.stdout.write(line)
        
    config = TopExamples.grid.Config()
    config.code          = 'top-xaod'
    config.settingsFile = CutFile 
    config.combine_outputFile = sel+'.root'


    config.gridUsername  = 'yajun' # use e.g. phys-top or phys-higgs for group production
    config.suffix        = sel+'_V1herwig'
    config.excludedSites = ''
    config.noSubmit      = False
    config.mergeType     = 'Default' #'None', 'Default' or 'xAOD'
    config.destSE        = '' #This is the default (anywhere), or try e.g. 'UKI-SOUTHGRID-BHAM-HEP_LOCALGROUPDISK'
    config.maxNFilesPerJob = '15'

    # by default the requested memory is set to 2GB, if you need to increase this, please disable the line below!!!
    #config.memory = '4000'
    #config.nameShortener = MyFancyShortener # to use your own physics part shortening function - uncomment here and in the function definition above

    #customTDPFile =
    config.checkPRW = True #this will find the PRW files in your cut files.
    #TopExamples.ami.check_sample_status(samples)  # Call with (samples, True) to halt on error
    TopExamples.grid.submit(config, samples)
