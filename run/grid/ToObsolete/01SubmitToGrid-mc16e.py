#!/usr/bin/env python

# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
import TopExamples.grid
import mc_p4128

## fancy function to shorten the physics part of the name of a sample
#def MyFancyShortener(superLongInDSName):
    #splitted = superLongInDSName.split('.')
    #runNumber = splitted[1]
    #physicsName = splitted[2]
    #if splitted[0] == "user" or splitted[0] == "group": #this is in case we run on private derivations, either produced with user or group role
        #runNumber = splitted[2]
        #physicsName = splitted[3]
    #tags = splitted[-1].replace('/','')
     #physicsName = physicsName.split('_')[0]
    #outDSName = runNumber + '.' + physicsName + '.someFunnyTags'
    #return outDSName

config = TopExamples.grid.Config()
config.code          = 'top-xaod'
config.settingsFile = 'Vjets-cuts-mc16e-0722.txt'
config.combine_outputFile = 'out.root'


config.gridUsername  = 'yajun' # use e.g. phys-top or phys-higgs for group production
config.suffix        = '1022'
config.excludedSites = ''
config.noSubmit      = False
config.mergeType     = 'Default' #'None', 'Default' or 'xAOD'
config.destSE        = '' #This is the default (anywhere), or try e.g. 'UKI-SOUTHGRID-BHAM-HEP_LOCALGROUPDISK'
config.maxNFilesPerJob = '2'
# by default the requested memory is set to 2GB, if you need to increase this, please disable the line below!!!
#config.memory = '7000' 
#config.nameShortener = MyFancyShortener # to use your own physics part shortening function - uncomment here and in the function definition above

#customTDPFile = 
config.checkPRW = True #this will find the PRW files in your cut files.


###############################################################################

###Command line interface
###If you want a script that ask you what you want to run on interactively,
###and uses lists of primary xAODs to convert them as TOPQ derivations
###Otherwise you can edit the automatic scripts below
#names, derivation, ptag = DerivationTags.InteractiveSubmission()
#samples = TopExamples.grid.Samples(names)
#TopExamples.grid.convertAODtoTOPQ(derivation, ptag, samples)
#TopExamples.grid.submit(config, samples)

###############################################################################



#names = ['Data-0','Data15-1','Data15-2','Data15-3','Data15-4','Data15-5'] 
#names = ['Data16-1','Data16-2','Data16-3','Data16-4','Data16-5','Data16-6','Data16-7','Data16-8','Data16-9']
#names = ['data17-1','data17-2','data17-3','data17-4','data17-5',"data17-6",'data17-7']
#names = ['data17-4','data17-5',"data17-6",'data17-7']
#names = ['data17-6']
#samples = TopExamples.grid.Samples(names)
#TopExamples.ami.check_sample_status(samples)  # Call with (samples, True) to halt on error
#TopExamples.grid.submit(config, samples)

###############################################################################

#names  = ["mc16e-resub"]
#names = ["mc16e-wqq","mc16e-zqq","mc16e-ttbar","mc16e-dijets"]
#names = ["mc16e-vqq-sherpa228","mc16e-vqq-sherpa228-lund","mc16e-dijets-FJ","mc16e-ttbar-aMcAtNloPy8"]
names = ["mc16e-vqq-sherpa228"]
#names = ["mc16e-ggh"]
#names = ["mc16e-ttbar"]
#names = ["mc16e-dijets"]
#"mc16e-wqq","mc16e-zqq"
#"mc16e-ttbar","mc16e-dijets"
#"mc16e-dijets-sherpa"
#"mc16e-dijets-sherpa-lund"
#"mc16e-dijets-herwig-dipole"
#"mc16e-dijets-herwig-angular"
#"mc16e-vqq-sherpa228"
#"mc16e-vqq-sherpa228-lund"
#"mc16e-dijets-FJ"
#"mc16e-ttbar-aMcAtNloPy8"
samples = TopExamples.grid.Samples(names)
#print samples
TopExamples.ami.check_sample_status(samples)  # Call with (samples, True) to halt on error
TopExamples.grid.submit(config, samples)
