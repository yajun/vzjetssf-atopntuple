#!/usr/bin/env python

# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
import TopExamples.grid
import mc_p4128

## fancy function to shorten the physics part of the name of a sample
#def MyFancyShortener(superLongInDSName):
    #splitted = superLongInDSName.split('.')
    #runNumber = splitted[1]
    #physicsName = splitted[2]
    #if splitted[0] == "user" or splitted[0] == "group": #this is in case we run on private derivations, either produced with user or group role
        #runNumber = splitted[2]
        #physicsName = splitted[3]
    #tags = splitted[-1].replace('/','')
     #physicsName = physicsName.split('_')[0]
    #outDSName = runNumber + '.' + physicsName + '.someFunnyTags'
    #return outDSName

config = TopExamples.grid.Config()
config.code          = 'top-xaod'
config.settingsFile = 'Vjets-cuts-mc16a-0722.txt'
config.combine_outputFile = 'out.root'


config.gridUsername  = 'yajun' # use e.g. phys-top or phys-higgs for group production
config.suffix        = '1022'
config.excludedSites = ''
config.noSubmit      = True
config.mergeType     = 'Default' #'None', 'Default' or 'xAOD'
config.destSE        = '' #This is the default (anywhere), or try e.g. 'UKI-SOUTHGRID-BHAM-HEP_LOCALGROUPDISK'
config.maxNFilesPerJob = '2'

# by default the requested memory is set to 2GB, if you need to increase this, please disable the line below!!!
#config.memory = '4000' 
#config.nameShortener = MyFancyShortener # to use your own physics part shortening function - uncomment here and in the function definition above

#customTDPFile = 
config.checkPRW = True #this will find the PRW files in your cut files.


names = ["mc16a-vqq-sherpa228"]
#names = ["mc16a-ggh"]
#names = ["mc16a-ttbar","mc16a-dijets"]
#names = ["mc16a-vqq-sherpa228","mc16a-vqq-sherpa228-lund","mc16a-ttbar-aMcAtNloPy8","mc16a-dijets-FJ"]
#names = ["mc16a-dijets-sherpa"]
#"mc16a-wqq","mc16a-zqq"
#"mc16a-ttbar","mc16a-dijets"
#"mc16a-dijets-sherpa"
#"mc16a-dijets-sherpa-lund"
#"mc16a-dijets-herwig-dipole"
#"mc16a-dijets-herwig-angular"
#"mc16a-vqq-sherpa228"
#"mc16a-vqq-sherpa228-lund"
#"mc16a-dijets-FJ"
#"mc16a-ttbar-aMcAtNloPy8"

samples = TopExamples.grid.Samples(names)
TopExamples.ami.check_sample_status(samples)  # Call with (samples, True) to halt on error
TopExamples.grid.submit(config, samples)
