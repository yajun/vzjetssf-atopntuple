#ifndef HbbCalibZbbChannel_EventSaver_H
#define HbbCalibZbbChannel_EventSaver_H

// AnalysisTop includes
#include "TopAnalysis/EventSaverBase.h"
#include "TopConfiguration/TopConfig.h"
#include "TopEventSelectionTools/TreeManager.h"
#include "TopCorrections/ScaleFactorRetriever.h"
#include "TopDataPreparation/SampleXsection.h"

// athena includes
#include "AsgTools/AsgTool.h"
#include "AsgTools/ToolHandle.h"
#include "AthContainers/AuxElement.h"
#include "xAODBase/IParticleContainer.h"

// FTag includes
#include "FTagAnalysisInterfaces/IBTaggingSelectionTool.h"
#include "FlavorTagDiscriminants/HbbTag.h"

// STL includes
#include <regex>
#include <functional>
#include <unordered_set>

// Boost includes
#include <boost/bimap.hpp>
//#include "BoostedJetTaggers/SmoothedWZTagger.h"

/**
 * @class top::HCZC::EventSaver
 *
 * Custom event saver to ensure that we only save what we actually want in the
 * output.
 */

#include <iostream>

namespace top { namespace HCZC {
  namespace detail {
    //auto configHasPhotons = [] (const top::TopConfig& config)
    //{ return config.usePhotons(); };
    //}
    //What objects are we using
    auto configHasJets = [] (const top::TopConfig& config)
      { return config.useJets(); };
  }
  static std::string ORSysts = "JET_.*|Muon_.*|EG_.*|TAU_.*|MET_.*"; 
  //static std::string ORSysts = "JET_.*|EG_.*";
  class EventSaver : public top::EventSaverBase, public asg::AsgTool 
  {
    public:
      /// Default constructor
      EventSaver();

      /// (Virtual) destructor
      virtual ~EventSaver();

      /**
       * @brief Initialize the event saver from the configuration.
       * @param config The configuration object.
       * @param file The output file in which the TTrees will be created.
       * @param extraBranches Extra info requested (e.g. which selections are
       * passed).
       */
      void initialize(
          std::shared_ptr<top::TopConfig> config,
          TFile* file,
          const std::vector<std::string>& extraBranches) override;

      /**
       * @brief The AsgTool initialize function.
       * We basically sidestep the EventLoop state machine (as far as I can see)
       * so this doesn't really do anything...
       */
      StatusCode initialize() override { return StatusCode::SUCCESS; }

      /**
       * @brief Retrieve/calculate the desired variables from the event.
       * @param event The top::Event to read from.
       *
       * This actually just stores the top::Event object for later when we can
       * process them all in concert!
       */
      void saveEvent(const top::Event& event) override;

      /**
       * @brief Actually process and save all of the previously stored
       * top::Event objects. Despite the name this has nothing to do with
       * writing and xAOD - this method is just reliably called at the end of
       * every event.
       */
      void saveEventToxAOD() override;

      // TODO truth event. Not sure about particle level event because it's not
      // clear what that *is*. Similarly, I don't know what the 'upgrade' event
      // is...

      /**
       * @brief finalize the saver - could be used to write out metadata?
       * This is also where we actually save the trees...
       */
      void finalize() override;
    protected:

      /**
       * @brief Get the selection decisions for a set of objects
       * @param[out] decisions The vector to hold the decisions.
       * @param selObjects Objects selected by this event.
       * @param indexMap Mapping from input index to output index.
       *
       * \ref selObjects holds all objects selected by *this* systematic. \ref
       * decisions is set so that it holds a char for each object in \ref
       * allObjects that says whether or not it is selected in this systematic.
       */
      template <typename T>
        void getSelectionDecisions(
            std::vector<char>& decisions,
            const DataVector<T>& selObjects,
            const boost::bimap<std::size_t, std::size_t>& indexMap);

      template <typename T>
	void getSelectionDecisions_lepton(
			     std::vector<char>& decisions,
			     const DataVector<T>& selObjects,
			     const boost::bimap<std::size_t, std::size_t>& indexMap);

      /**
       * @brief Create the view container that holds all objects selected by any
       * systematic on this event.
       *
       * @tparam T Container type
       * @param key The store gate key for this systematic.
       * @param indexMap A mapping between input (left) and output (right) index
       */
      template <typename T>
        ConstDataVector<T> getSelectedContainer(
            const std::string& key,
            const boost::bimap<std::size_t, std::size_t>& indexMap);

      /**
       * @brief Generic base class for branch classes
       */
      class BranchBase {
        public:
          /**
           * @brief Helper function that always returns true
           */
          static bool always(const top::TopConfig&) { return true; }
          /**
           * @brief Helper function that retrieves whether or not this is an MC
           * sample.
           */
          static bool isMC(const top::TopConfig& config)
          { return config.isMC(); }

          /**
           * @brief Helper function that retrieves whether or not this is a data
           * sample
           */
          static bool isData(const top::TopConfig& config)
          { return !config.isMC(); }
          /**
           * @brief Constructor
           * @param parent The parent EventSaver (this object!)
           * @param name The name of this branch in the output trees.
           * @param systRegex The pattern deciding which systematics affect
           *        this.
           * @param activateIf A function that determines from a configuration if
           *        this branch should be active.
           *
           * The parent is only used to add this branch to the EventSaver's list
           * of branches.
           */
          BranchBase(
              EventSaver* parent,
              const std::string& name,
              const std::regex& systRegex,
              std::function<bool(const top::TopConfig&)> activateIf);


          /// Copying branches makes no sense - it would result in a name
          /// conflict in the output TTree.
          BranchBase(const BranchBase& other) = delete;

          /// Move constructor deregisters old branch and registers this one.
          BranchBase(BranchBase&& other);

          /// Destructor deregisters this
          virtual ~BranchBase() { deregisterThis(); }

          /// The branch name
          const std::string& name() { return m_name; }

          /**
           * @brief Explicitly provide a list of systematics. 
           * @param systs The systematics, provided as a set of their hash
           *        values.
           * If this is used then the regex will be ignored. The hash values
           * should be retrieved from the top config. This will throw an error
           * if the internal systematics have already been set, either by this
           * method or through the regex.
           */
          void provideSystematics(const std::set<std::size_t>& systs);

          /**
           * @brief Prepare the list of systematics.
           * This will do nothing if the systematics have already been provided
           * through the \ref provideSystematics method.
	   * @param nominalHash The hash value of the nominal syst
	   * @param allSysts Has values and names of all systematics
           */
          void compileSystematics(
				  std::size_t nominalHash,
				  const std::vector<std::pair<std::size_t, std::string>>& allSysts);

          /// Is this branch affected by a systematic. Only valid once the
          /// systematics have been loaded.
          bool isAffectedBy(std::size_t systHash) const
          { return m_systs.count(systHash) == 1; }

          /// Is this active?
          bool isActive();

          /**
           * @brief Add this branch to a TTree
           * @param systHash The systematic hash associated to this tree.
           * @param tree The tree to add the branch to.
           */
          virtual void addToTree(
              std::size_t systHash,
              TreeManager& tree) = 0;

          /**
           * @brief Reset the value held by this branch.
           * This is called at the start of each event to ensure that old values
           * from previous events are not carried over.
           */
          virtual void reset() = 0;
        protected:
          /// Has this already been added to a tree?
          bool m_isInTree{false};
          /// Register this object with the parent
          void registerThis();
          /// Deregister this object from the parent
          void deregisterThis();

          /// The parent
          EventSaver* m_parent;
          /// The branch name
          std::string m_name;
          /// The regex to determine which systematics to use.
          std::regex m_systRegex;
          /// The function to test if this is active or not
          std::function<bool(const top::TopConfig&)> m_activateIf;
          /// The set of all systematics that affect us.
          std::set<std::size_t> m_systs;
      }; //> end class BranchBase

      /**
       * @brief Helper class to hold branches for a tree. The branch holds all
       * the values that correspond to all relevant systematic variations.
       * @tparam T The type of the branch.
       */
      template <typename T>
        class Branch : public BranchBase
      {
        public:
          /**
           * @brief Constructor for a default constructible type.
           * @param parent The parent EventSaver (this object!)
           * @param name The name of this branch in the output trees.
           * @param systRegex The pattern deciding which systematics affect
           *        this. 
           * @param activateIf A function that determines from a configuration if
           *        this branch should be active.
           * This version is only selected for a type that is default
           * constructible. The default constructed quantity is then used as
           * the branch default.
           */
          template <typename=
            std::enable_if_t<std::is_default_constructible<T>{}, void>>
            Branch(
                EventSaver* parent,
                const std::string& name,
                const std::string& systRegex = "^$",
                std::function<bool(const top::TopConfig&)> activateIf =
                  BranchBase::always)
            : Branch(T(), parent, name, systRegex, activateIf) {}

          /**
           * @brief Constructor with a default value.
           * @param defaultValue The default value for this branch.
           * @param parent The parent EventSaver (this object!)
           * @param name The name of this branch in the output trees.
           * @param systRegex The pattern deciding which systematics affect
           *        this.
           * @param activateIf A function that determines from a configuration if
           *        this branch should be active.
           * This version is only selected for a type that is default
           * constructible. The default constructed quantity is then used as
           * the branch default.
           */
          Branch(
              const T& defaultValue,
              EventSaver* parent,
              const std::string& name,
              const std::string& systRegex = "^$",
              std::function<bool(const top::TopConfig&)> activateIf = 
                BranchBase::always)
            : BranchBase(parent, name, std::regex(systRegex), activateIf),
              m_default(defaultValue) {}

          /**
           * @brief Add this branch to a TTree
           * @param systHash The hash of this systematic.
           * @param tree The tree to add the branch to.
           * The branch is only added if the systematic affects it.
           */
          void addToTree(
              std::size_t systHash,
              TreeManager& tree) override
          {
            m_isInTree = true;
            if (isAffectedBy(systHash) ) {
              m_values.insert(std::make_pair(systHash, m_default) );
              tree.makeOutputVariable(m_values.at(systHash), name() );
            }
          }

          /**
           * @brief Reset the value held by this branch.
           * This is called at the start of each event to ensure that old values
           * from previous events are not carried over.
           */
          void reset() override
          {
            for (auto& valPair : m_values)
              valPair.second = m_default;
          }

          /// Get the value
          T& value(std::size_t systHash);
        private:
          /// The default value
          T m_default;
          /// The values (one per systematic)
          std::map<std::size_t, T> m_values;
      }; // end class Branch

      std::vector<BranchBase*> m_allBranches;

      /// The configuration
      std::shared_ptr<top::TopConfig> m_config;

      //The hbbTagTool
      std::unique_ptr<FlavorTagDiscriminants::HbbTag> m_hbbTag;

      /// The output file (non-owning pointer)
      TFile* m_fOut{nullptr};

      /// The 'sf-retriever'
      top::ScaleFactorRetriever* m_sfRetriever;

      /// Retrieve XSec info
      SampleXsection m_tdpXSec;

      /// The tree managers.
      std::vector<std::shared_ptr<top::TreeManager>> m_treeManagers;
      std::shared_ptr<top::TreeManager> m_metaTreeManager;

      bool m_firstEvent{true};

      /// The top events that we've stored up.
      std::vector<top::Event> m_events;

      /// The names of the decision branches
      std::vector<std::string> m_selectionDecisionNames;

      /// Here we put all of the branches that correspond to an event selection.
      // The decisions are decorated onto the event info so we need an accessor
      // to pull them back out.
      std::vector<std::pair<
        SG::AuxElement::ConstAccessor<int>,
        Branch<bool>>> m_selectionBranches;

      /// For each object, record whether or not they're selected in *any*
      /// systematic
      std::set<std::size_t> m_jetMask;
      std::set<std::size_t> m_eleMask;
      std::set<std::size_t> m_gamMask;
      std::set<std::size_t> m_muMask;
      std::set<std::size_t> m_tauMask;
      std::set<std::size_t> m_metMask;
      std::set<std::size_t> m_lrjMask;
      std::set<std::size_t> m_tlrjMask;
      std::set<std::size_t> m_tjMask;

      /// From here, we have the branches themselves
      Branch<uint32_t> m_runNumber{this, "RunNumber", "^$", BranchBase::isData};
      Branch<uint32_t> m_dsid{this, "DSID", "^$", BranchBase::isMC};
      Branch<unsigned long long> m_eventNumber{this, "EventNumber", "^$"};
      Branch<uint32_t> m_lumiBlock{this, "LumiBlock", "^$"};
      //      Branch<float> m_pileupWeight{this,"PileupWeight","PRW_DATASF.*", BranchBase::isMC};
      Branch<float> m_prwEventWeight{this, "PRWEventWeight", "PRW_DATASF.*", BranchBase::isMC};
      Branch<float> m_mcEventWeight{this, "EventWeight", "^$", BranchBase::isMC};
      Branch<float> m_jvtEventWeight{this, "JVTEventWeight", "JET_JvtEfficiency.*|"+ORSysts, BranchBase::isMC};
      Branch<float> m_averagePileup{this, "AverageInteractionsPerBunchCrossing", "^$"};
      Branch<float> m_actualPileup{this, "ActualInteractionsPerBunchCrossing", "^$"};

      // TODO - JVT, electron ID, muon ID scale factors. Given that we veto on
      // electrons/muons I wonder if we dont' need them (but perhaps we do if
      // they participate in OR?)


      /// Jet branches
      Branch<std::vector<char>> m_jetIsSelected{this, "JetIsSelected", ORSysts, detail::configHasJets};
      /// If this jet is selected in the 'loose' variation (i.e. passes OR requirements from loose objects.
      Branch<std::vector<char>> m_jetIsSelectedLoose{this, "JetIsSelectedLoose", ORSysts,detail::configHasJets};
      Branch<std::vector<float>> m_jetPt{this, "JetPt", "JET_.*", detail::configHasJets};
      Branch<std::vector<float>> m_jetEta{this, "JetEta", "JET_.*", detail::configHasJets};
      Branch<std::vector<float>> m_jetPhi{this, "JetPhi", "JET_.*", detail::configHasJets};
      Branch<std::vector<float>> m_jetMass{this, "JetMass", "JET_.*", detail::configHasJets};
      Branch<std::vector<float>> m_jetHadronLabel{this, "JetHadronConeExclTruthLabelID", "^$", BranchBase::isMC};

      // NB - I've not included the discriminants as I guess we're not going to
      // be looking at anything beyond the decisions for each supported WP. If
      // we want to change that it's not too hard.

      // Having chatted with Chris (P) he pointed out that we are deriving
      // flavour-tagging scale factors here so we don't need those systematics.


      /// B-tagging branches
      /// R=0.4 branches
      std::vector<std::pair<
        SG::AuxElement::ConstAccessor<char>,
        Branch<std::vector<char>>>> m_jetIsBTagged;

      std::vector<std::pair<
        ToolHandle<IBTaggingSelectionTool>,
        Branch<std::vector<char>>>> m_trackJetIsBTagged;

      /// VR track jet branches
      // std::vector<std::pair<
      //  SG::AuxElement::ConstAccessor<char>,
      //  Branch<std::vector<char>>>> m_trackJetIsBTagged;

      /// Track jet branches
      // What systematics affect these!? - supposedly none but that's just
      // because they aren't actually measured. This means that we have to be
      // very careful on any selections on track-jet kinematics. I think we can
      // largely hope that selections on track-jet b-tagging is fine as that
      // *is* calibrated.
      //
      // Also, a note on the third argument here. As we have no systematics that
      // we know are affecting these we stick in a regex that will match only
      // the empty string - ^ means match the start of the line, $ means match
      // the end. This should mean that these branches only appear in the
      // nominal tree.
      Branch<std::vector<char>> m_tjetIsSelected{this, "TrackJetIsSelected", "^$"}; 
      Branch<std::vector<float>> m_tjetPt{this, "TrackJetPt", "^$"};
      Branch<std::vector<float>> m_tjetEta{this, "TrackJetEta", "^$"};
      Branch<std::vector<float>> m_tjetPhi{this, "TrackJetPhi", "^$"};
      Branch<std::vector<float>> m_tjetMass{this, "TrackJetMass", "^$"};
      Branch<std::vector<float>> m_tjetHadronLabel{this, "TrackJetHadronConeExclTruthLabelID", "^$", BranchBase::isMC};
      // This is important. For any track jet that we test the b-tagging for we
      // should also check this variable. If it fails this variable we have to
      // fail the *entire* event.
      Branch<std::vector<char>> m_tjetPassDRCut{this, "TrackJetPassDR", "^$"};
      Branch<std::vector<float>> m_tjetDL1pb{this, "TrackJetDL1pb", "^$"};
      Branch<std::vector<float>> m_tjetDL1pc{this, "TrackJetDL1pc", "^$"};
      Branch<std::vector<float>> m_tjetDL1pu{this, "TrackJetDL1pu", "^$"};
      Branch<std::vector<float>> m_tjetDL1rpb{this, "TrackJetDL1rpb", "^$"};
      Branch<std::vector<float>> m_tjetDL1rpc{this, "TrackJetDL1rpc", "^$"};
      Branch<std::vector<float>> m_tjetDL1rpu{this, "TrackJetDL1rpu", "^$"};
      Branch<std::vector<float>> m_tjetDL1rmupb{this, "TrackJetDL1rmupb", "^$"};
      Branch<std::vector<float>> m_tjetDL1rmupc{this, "TrackJetDL1rmupc", "^$"};
      Branch<std::vector<float>> m_tjetDL1rmupu{this, "TrackJetDL1rmupu", "^$"};

      Branch<std::vector<std::vector<std::size_t>>> m_tjAssociatedTrackParticles{
         this, "TrackjetAssociatedTrackParticles", "^$"};


      /// Electron branches
      Branch<std::vector<char>> m_eleIsSelected{this, "ElectronIsSelected", ORSysts};
      Branch<std::vector<char>> m_eleIsSelectedLoose{this, "ElectronIsSelectedLoose", ORSysts};
      Branch<std::vector<float>> m_elePt{this, "ElectronPt", "EG_.*"};
      Branch<std::vector<float>> m_eleEta{this, "ElectronEta", "EG_.*"};
      Branch<std::vector<float>> m_elePhi{this, "ElectronPhi", "EG_.*"};
      Branch<std::vector<float>> m_eleCharge{this, "ElectronCharge", "^$"};

      /// Photon branches (we don't need photon information in Z+jets calibration)
      //Branch<std::vector<char>> m_gamIsSelected{this, "PhotonIsSelected", ORSysts, detail::configHasPhotons};
      //Branch<std::vector<char>> m_gamIsSelectedLoose{this, "PhotonIsSelectedLoose", ORSysts, detail::configHasPhotons};
      //Branch<std::vector<float>> m_gamPt{this, "PhotonPt", "EG_.*", detail::configHasPhotons};
      //Branch<std::vector<float>> m_gamEta{this, "PhotonEta", "EG_.*", detail::configHasPhotons};
      //Branch<std::vector<float>> m_gamPhi{this, "PhotonPhi", "EG_.*", detail::configHasPhotons};

      /// Muon branches
      Branch<std::vector<char>> m_muIsSelected{this, "MuonIsSelected", ORSysts};
      Branch<std::vector<char>> m_muIsSelectedLoose{this, "MuonIsSelectedLoose", ORSysts};
      Branch<std::vector<float>> m_muPt{this, "MuonPt", "MUON_.*"};
      Branch<std::vector<float>> m_muEta{this, "MuonEta", "MUON_.*"};
      Branch<std::vector<float>> m_muPhi{this, "MuonPhi", "MUON_.*"};
      Branch<std::vector<unsigned short>> m_muType{this, "MuonType", "MUON_.*"};
      Branch<std::vector<unsigned char>> m_muQuality{this, "MuonQuality", "MUON_.*"};
      Branch<std::vector<float>> m_muCharge{this, "MuonCharge", "^$"};
      Branch<std::vector<float>> m_muEnergyLoss{this, "MuonEnergyLoss", "^$"};

      /// Tau branches
      Branch<std::vector<char>> m_tauIsSelected{this, "TauIsSelected", ORSysts};
      Branch<std::vector<char>> m_tauIsSelectedLoose{this, "TauIsSelectedLoose", ORSysts};
      Branch<std::vector<float>> m_tauMass{this, "TauMass", "TAU_.*"};
      Branch<std::vector<float>> m_tauPt{this, "TauPt", "TAU_.*"};
      Branch<std::vector<float>> m_tauEta{this, "TauEta", "TAU_.*"};
      Branch<std::vector<float>> m_tauPhi{this, "TauPhi", "TAU_.*"};
      Branch<std::vector<float>> m_tauCharge{this, "TauCharge", "^$"};

      /// MET branches
      Branch<std::vector<float>> m_metMet{this, "MetMet","MET_.*"};
      Branch<std::vector<float>> m_metPhi{this, "MetPhi","MET_.*"};

      // Large-R jet branches
      Branch<std::vector<char>> m_lrjIsSelected{this, "LargeRJetIsSelected", ".*JET_.*"};
      Branch<std::vector<float>> m_lrjPt{this, "LargeRJetPt", ".*JET_.*"};
      Branch<std::vector<float>> m_lrjEta{this, "LargeRJetEta", ".*JET_.*"};
      Branch<std::vector<float>> m_lrjPhi{this, "LargeRJetPhi", ".*JET_.*"};
      Branch<std::vector<float>> m_lrjMass{this, "LargeRJetMass", ".*JET_.*"};

      Branch<std::vector<float>> m_lrjCaloMass{this, "LargeRJetCaloMass", "^$"};
      Branch<std::vector<float>> m_lrjCaloPt{this, "LargeRJetCaloPt", "^$"};
      Branch<std::vector<float>> m_lrjCaloEta{this, "LargeRJetCaloEta", "^$"};
      Branch<std::vector<float>> m_lrjCaloPhi{this, "LargeRJetCaloPhi", "^$"};

      Branch<std::vector<float>> m_lrjTAMass{this, "LargeRJetTAMass", "^$"};
      Branch<std::vector<float>> m_lrjTAPt{this, "LargeRJetTAPt", "^$"};
      Branch<std::vector<float>> m_lrjTAEta{this, "LargeRJetTAEta", "^$"};
      Branch<std::vector<float>> m_lrjTAPhi{this, "LargeRJetTAPhi", "^$"};

      Branch<std::vector<float>> m_lrjTruthMass{this, "LargeRJetTruthMass", ".*JET_.*", BranchBase::isMC};
      Branch<std::vector<float>> m_lrjTruthPt{this, "LargeRJetTruthPt", ".*JET_.*", BranchBase::isMC};
      Branch<std::vector<float>> m_lrjTruthEta{this, "LargeRJetTruthEta", ".*JET_.*", BranchBase::isMC};
      Branch<std::vector<float>> m_lrjTruthPhi{this, "LargeRJetTruthPhi", ".*JET_.*", BranchBase::isMC};

      Branch<std::vector<int>> m_lrjTruthLabel{this, "LargeRJetTruthLabel", ".*JET_.*", BranchBase::isMC};
      Branch<std::vector<float>> m_lrjTruthLabel_dR_W{this, "LargeRJetdR_W", ".*JET_.*", BranchBase::isMC};
      Branch<std::vector<float>> m_lrjTruthLabel_dR_Z{this, "LargeRJetdR_Z", ".*JET_.*", BranchBase::isMC};
      Branch<std::vector<float>> m_lrjTruthLabel_dR_H{this, "LargeRJetdR_H", ".*JET_.*", BranchBase::isMC};
      Branch<std::vector<float>> m_lrjTruthLabel_dR_Top{this, "LargeRJetdR_Top", ".*JET_.*", BranchBase::isMC};
      Branch<std::vector<int>> m_lrjTruthLabel_dR_NB{this, "LargeRJetdR_NB", ".*JET_.*", BranchBase::isMC};

      Branch<std::vector<float>> m_lrjXbbTop{this, "LargeRJetXbbScoreTop", ".*JET_.*"};
      Branch<std::vector<float>> m_lrjXbbQCD{this, "LargeRJetXbbScoreQCD", ".*JET_.*"};
      Branch<std::vector<float>> m_lrjXbbHiggs{this, "LargeRJetXbbScoreHiggs", ".*JET_.*"};


      Branch<std::vector<int>> m_lrjPassWNtrk_50{this, "LargeRJetPassWNtrk_50",".*JET_.*"};
      Branch<std::vector<int>> m_lrjPassWD2_50{this, "LargeRJetPassWD2_50",".*JET_.*"};
      Branch<std::vector<int>> m_lrjPassWMassLow_50{this,"LargeRJetPassWMassLow_50",".*JET_.*"};
      Branch<std::vector<int>> m_lrjPassWMassHigh_50{this,"LargeRJetPassWMassHigh_50",".*JET_.*"};

      Branch<std::vector<int>> m_lrjPassZNtrk_50{this, "LargeRJetPassZNtrk_50",".*JET_.*"};
      Branch<std::vector<int>> m_lrjPassZD2_50{this, "LargeRJetPassZD2_50",".*JET_.*"};
      Branch<std::vector<int>> m_lrjPassZMassLow_50{this,"LargeRJetPassZMassLow_50",".*JET_.*"};
      Branch<std::vector<int>> m_lrjPassZMassHigh_50{this,"LargeRJetPassZMassHigh_50",".*JET_.*"};


      Branch<std::vector<int>> m_lrjPassWNtrk_80{this, "LargeRJetPassWNtrk_80",".*JET_.*"};
      Branch<std::vector<int>> m_lrjPassWD2_80{this, "LargeRJetPassWD2_80",".*JET_.*"};
      Branch<std::vector<int>> m_lrjPassWMassLow_80{this,"LargeRJetPassWMassLow_80",".*JET_.*"};
      Branch<std::vector<int>> m_lrjPassWMassHigh_80{this,"LargeRJetPassWMassHigh_80",".*JET_.*"};

      Branch<std::vector<int>> m_lrjPassZNtrk_80{this, "LargeRJetPassZNtrk_80",".*JET_.*"};
      Branch<std::vector<int>> m_lrjPassZD2_80{this, "LargeRJetPassZD2_80",".*JET_.*"};
      Branch<std::vector<int>> m_lrjPassZMassLow_80{this,"LargeRJetPassZMassLow_80",".*JET_.*"};
      Branch<std::vector<int>> m_lrjPassZMassHigh_80{this,"LargeRJetPassZMassHigh_80",".*JET_.*"};


      Branch<std::vector<float>> m_lrjHbbTop{this, "LargeRJetHbbScoreTop", ".*JET_.*"};
      Branch<std::vector<float>> m_lrjHbbQCD{this, "LargeRJetHbbScoreQCD", ".*JET_.*"};
      Branch<std::vector<float>> m_lrjHbbHiggs{this, "LargeRJetHbbScoreHiggs", ".*JET_.*"};
      Branch<std::vector<float>> m_lrjNtrk{this, "LargeRJetNtrk", ".*JET_.*"};

      Branch<std::vector<float>> m_lrjNtrkPt500{this, "LargeRJetNtrkPt500", ".*JET_.*"};

      Branch<std::vector<float>> m_lrjECF1{this, "LargeRJetECF1", ".*JET_.*"};
      Branch<std::vector<float>> m_lrjECF2{this, "LargeRJetECF2", ".*JET_.*"};
      Branch<std::vector<float>> m_lrjECF3{this, "LargeRJetECF3", ".*JET_.*"};
      Branch<std::vector<float>> m_lrjD2{this, "LargeRJetD2", ".*JET_.*"};
      Branch<std::vector<float>> m_lrjTau1_wta{this, "LargeRJetTau1_wta", ".*JET_.*"};
      Branch<std::vector<float>> m_lrjTau2_wta{this, "LargeRJetTau2_wta", ".*JET_.*"};
      Branch<std::vector<float>> m_lrjTau3_wta{this, "LargeRJetTau3_wta", ".*JET_.*"};
      Branch<std::vector<float>> m_lrjTau21{this, "LargeRJetTau21", ".*JET_.*"};
      Branch<std::vector<float>> m_lrjFoxWolfram2{this, "LargeRJetFoxWolfram2", "^$"};
      Branch<std::vector<float>> m_lrjFoxWolfram1{this, "LargeRJetFoxWolfram1", "^$"};
      Branch<std::vector<float>> m_lrjFoxWolfram0{this, "LargeRJetFoxWolfram0", "^$"};

      Branch<std::vector<float>> m_lrjSplit12{this, "LargeRJetSplit12", ".*JET_.*"};
      Branch<std::vector<float>> m_lrjSplit23{this, "LargeRJetSplit23", ".*JET_.*"};
      Branch<std::vector<float>> m_lrjQw{this, "LargeRJetQw","^$"};
      Branch<std::vector<float>> m_lrjPlanarFlow{this, "LargeRJetPlanarFlow", "^$"};
      Branch<std::vector<float>> m_lrjAngularity{this, "LargeRJetAngularity", "^$"};
      Branch<std::vector<float>> m_lrjAplanarity{this, "LargeRJetAplanarity", "^$"};
      Branch<std::vector<float>> m_lrjZCut12{this, "LargeRJetZCut12", "^$"};
      Branch<std::vector<float>> m_lrjKtDR{this, "LargeRJetKtDR", "^$"};
      Branch<std::vector<float>> m_lrjThrustMin{this, "LargeRJetThrustMin", "^$"};
      Branch<std::vector<float>> m_lrjThrustMaj{this, "LargeRJetThrustMaj", "^$"};
      Branch<std::vector<float>> m_lrjSphericity{this, "LargeRJetSphericity", "^$"};
      Branch<std::vector<float>> m_lrjMu12{this, "LargeRJetMu12", "^$"};

      // This branch contains the index in the TrackJet* branches of any
      // ghost-associated track jets. This is based on the jet building and is
      // not affected by any systematics.
      Branch<std::vector<std::vector<std::size_t>>> m_lrjAssociatedTrackJets{
        this, "LargeRJetAssociatedTrackJet", "^$"};


      ClassDefOverride(top::HCZC::EventSaver, 0);

  }; //> end class EventSaver
} /*> end namespace HCZC */ } /*> end namespace top */

#endif //> !HbbCalibZbbChannel_EventSaver_H
