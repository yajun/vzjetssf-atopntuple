
// package includes
#include "HbbCalibZbbChannel/EventSaver.h"

// AnalysisTop includes
#include "TopEvent/Event.h"
#include "TopEvent/EventTools.h" // For top::check

// athena includes
#include "AsgTools/AsgTool.h"
#include "AsgTools/ToolHandle.h"
#include "AthContainers/AuxElement.h"
#include "xAODBase/IParticleContainer.h"
#include "xAODTracking/TrackParticleContainer.h"

// JetEtMiss includes
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingET.h"

// FTag includes
#include "xAODBTaggingEfficiency/BTaggingSelectionTool.h"
#include "FlavorTagDiscriminants/HbbTagConfig.h"// for hbbtag
#include "FlavorTagDiscriminants/HbbTag.h"
#include "PATCore/TAccept.h"
#include "lwtnn/LightweightGraph.hh"

#include "PathResolver/PathResolver.h"

// ROOT includes
#include <TFile.h>
 

// Anonymous namespace contains helpers
namespace {
  static SG::AuxElement::ConstAccessor<
    std::vector<ElementLink<xAOD::IParticleContainer>>
    > accTrackJets("GhostVR30Rmax4Rmin02TrackJet_BTagging201903");
  static SG::AuxElement::ConstAccessor<ElementLink<xAOD::JetContainer>>
    accParent("Parent");
  static SG::AuxElement::ConstAccessor<std::vector<ElementLink<xAOD::IParticleContainer>> > accConstituent("constituentLinks");

  boost::bimap<std::size_t, std::size_t> constructIndexMap(const std::set<std::size_t>& s)
  {
    using bm_type = boost::bimap<std::size_t, std::size_t>;
    bm_type ret;
    std::size_t outIdx = 0;
    auto itr = s.begin();
    for (; itr != s.end(); ++itr, ++outIdx)
      ret.insert(bm_type::value_type(*itr, outIdx) );
    return ret;
  }

  const static CP::SystematicVariation PRWWeightUp("PRW_DATASF", 1);
  const static CP::SystematicVariation PRWWeightDown("PRW_DATASF", -1);
  const static CP::SystematicVariation JVTWeightUp("JET_JvtEfficiency", 1);
  const static CP::SystematicVariation JVTWeightDown("JET_JvtEfficiency", -1);

  // Substructure functions
  /// get tau_{i,i-1}
  /*template <std::size_t I>
  float tauIJ(const xAOD::Jet& j)
  {
    static SG::AuxElement::ConstAccessor<float> tauI("Tau"+std::to_string(I)+"_wta");
    static SG::AuxElement::ConstAccessor<float> tauIm1("Tau"+std::to_string(I-1)+"_wta");
    return tauI(j)/tauIm1(j);
  }

  // Get D2 (beta = 1)
  float D2(const xAOD::Jet& j)
  {
    static SG::AuxElement::ConstAccessor<float> e3("ECF3");
    static SG::AuxElement::ConstAccessor<float> e2("ECF2");
    static SG::AuxElement::ConstAccessor<float> e1("ECF1");
    if (e2(j) < 1e-8) 
      // prevent 0 division
      return -999;
    return e3(j)*std::pow(e1(j), 3)/std::pow(e2(j), 3);
    }*/

  // Split a string on the first instance of the supplied substring and return
  // the part before the substring, the substring and the part after it
  std::array<std::string, 3> partition(std::string inStr, std::string subStr)
  {
    std::size_t pos = inStr.find(subStr);
    if (pos == std::string::npos)
      // not found
      return {inStr, "", ""};
    else
      return {
        inStr.substr(0, pos),
          inStr.substr(pos, subStr.size()),
          inStr.substr(pos+subStr.size())
	  };

  }

  ToolHandle<IBTaggingSelectionTool> makeBTagTool(
						  std::string cdi,
						  std::string tagger,
						  std::string op,
						  std::string author,
						  float minPt)
  {
    auto tool = std::make_unique<BTaggingSelectionTool>(
							"BTag_"+author+"_"+op+"_"+tagger);
    top::check(tool->setProperty("FlvTagCutDefinitionsFileName", cdi), "Failed to set cdi");
    top::check(tool->setProperty("TaggerName", tagger), "Failed to set tagger");
    top::check(tool->setProperty("OperatingPoint", op), "Failed to set op");
    top::check(tool->setProperty("JetAuthor", author), "Failed to set author");
    top::check(tool->setProperty("MinPt", minPt), "Failed to set minPt");
    top::check(tool->initialize(), "Failed to initialize BTag tool!");
    return tool.release();
  }

} //> end anonymous namespace

namespace top { namespace HCZC {

  EventSaver::EventSaver() :
    asg::AsgTool( "top::HCZC::EventSaver" )
  {}

  EventSaver::~EventSaver() {}

  void EventSaver::initialize(
      std::shared_ptr<top::TopConfig> config,
      TFile* file,
      const std::vector<std::string>& extraBranches)
  {
    m_config = config;
    m_fOut = file;

    struct FlavorTagDiscriminants::HbbTagConfig hbbConfig("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/BTagging/202002/GhostVR30Rmax4Rmin02TrackJet_BTagging201903/network.json");
    m_hbbTag = std::make_unique<FlavorTagDiscriminants::HbbTag>(hbbConfig);

    if (m_config->isMC()) {
      if (asg::ToolStore::contains<ScaleFactorRetriever>("top::ScaleFactorRetriever")) {
        m_sfRetriever = asg::ToolStore::get<ScaleFactorRetriever>("top::ScaleFactorRetriever");
      } else {
	top::ScaleFactorRetriever* topSFR = new top::ScaleFactorRetriever("top::ScaleFactorRetriever");
	top::check(asg::setProperty(topSFR, "config", m_config), "Failed to set config");
	top::check(topSFR->initialize(), "Failed to initalialise");
        m_sfRetriever = topSFR;
      }
    }
    m_selectionDecisionNames = extraBranches;   
    // Make sure that my assumptions about our settings hold true.
    top::check(!m_config->applyElectronInJetSubtraction(),
        "This code just can't cope with the electron in jet subtraction right now :(");
    // Initialise the selection branches. Sadly I can't think of a way to check
    // which systematics affect them so they get written out separately for
    // each.
    for (const std::string& selection : m_selectionDecisionNames)
      m_selectionBranches.push_back(std::make_pair(
            SG::AuxElement::ConstAccessor<int>(selection),
            Branch<bool>(this, selection, ".*") ) );    

    // Now prepare the b-tagging branches.
    for (const auto& wp : m_config->bTagWP_available() )
      m_jetIsBTagged.push_back(std::make_pair(
            SG::AuxElement::ConstAccessor<char>("isbtagged_"+wp),
            Branch<std::vector<char>>(this, "JetIsBTagged_"+wp, "^$") ) );
 
    /*for (const auto& wp : m_config->bTagWP_available() )
      m_trackJetIsBTagged.push_back(std::make_pair(
            SG::AuxElement::ConstAccessor<char>("isbtagged_"+wp),
            Branch<std::vector<char>>(this, "TrackJetIsBTagged_"+wp, "^$") ) );*/
    
    //comment b-tagging part
    for (const auto& wp : m_config->bTagWP_available() ) {
      std::array<std::string, 3> split = partition(wp, "_");
      m_trackJetIsBTagged.push_back(std::make_pair(
						   makeBTagTool(
								m_config->bTaggingCDIPath(),
								split.at(0),
								split.at(2),
								"AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903",
								7000),
						   Branch<std::vector<char>>(this, "TrackJetIsBTagged_"+wp, "^$") ) );
      top::check(m_trackJetIsBTagged.back().first.retrieve(), "Failed to retrieve tool");
      }// end for (const auto& wp : m_config->bTagWP_available() )
    
    std::size_t nominalHash = m_config->nominalHashValue();
    std::vector<std::pair<std::size_t, std::string>> allSysts;
    std::vector<std::string> extraSysts {
        PRWWeightUp.name(),
        PRWWeightDown.name(),
        JVTWeightUp.name(),
        JVTWeightDown.name(),
	};
    allSysts.reserve(m_config->systAllTTreeNames()->size() + extraSysts.size() );
    std::transform(
		   m_config->systAllTTreeNames()->begin(), m_config->systAllTTreeNames()->end(),
		   std::back_inserter(allSysts),
		   [this] (const std::pair<std::size_t, std::string>& p)
		   { return std::make_pair(p.first, m_config->systematicName(p.first));});

    std::transform(
		   extraSysts.begin(), extraSysts.end(),
		   std::back_inserter(allSysts),
		   [this] (const std::string& name)
		   { return std::make_pair(std::hash<std::string>{}(name), name); });

    ATH_MSG_INFO("Will consider systematics: ");
    for (const auto& p : allSysts)
      ATH_MSG_INFO("\t" << p.second);

    // Go through the branches, removing any (from this vector, not the actual
    // branch itself) if they aren't active.
    auto itr = m_allBranches.begin();
    while (itr != m_allBranches.end() ) {
      if (!(*itr)->isActive() )
        itr = m_allBranches.erase(itr);
      else {
        (*itr)->compileSystematics(nominalHash,allSysts);
        ++itr;
      }
    }

    // Prepare the vector of events.
    std::size_t nSysts = allSysts.size();
    // The number of systematics from AnalysisTop
    std::size_t nATSysts = m_config->systAllTTreeNames()->size();


    // We need one event per loose/tight * nSyst combination
    top::check(!config->doLooseEvents(), "Loose events are no longer supported");
    m_events.resize(nATSysts * (
          int(config->doLooseEvents() ) + int(config->doTightEvents() ) ) );

    // Now we create the TTrees - one per systematic
    m_treeManagers.reserve(nSysts);
    std::string nominalTreeName;
    // The tree names are stored as a shared_ptr to a map - why? Who knows.
    for (const auto& treeNamePair : allSysts) {
      ATH_MSG_INFO("Tree name: " << treeNamePair.second);
      // The first part of the pair is a hash which has to be converted back to
      // a string
      m_treeManagers.push_back(std::make_shared<top::TreeManager>(
            treeNamePair.second,
            file,
            config->outputFileNEventAutoFlush(),
            config->outputFileBasketSizePrimitive(),
            config->outputFileBasketSizeVector() ) );
      if (treeNamePair.first == config->nominalHashValue() )
        nominalTreeName = treeNamePair.second;
      for (BranchBase* branch : m_allBranches)
        branch->addToTree(treeNamePair.first, *m_treeManagers.back() );
    } // end for (const auto& treeNamePair : allSysts)

    if (m_config->isMC() )
      m_metaTreeManager = std::make_shared<top::TreeManager>(
							     "SampleMetaData",
							     file,
							     config->outputFileNEventAutoFlush(),
							     config->outputFileBasketSizePrimitive(),
							     config->outputFileBasketSizeVector() );

    
  }

  void EventSaver::saveEvent(const top::Event& event) 
  {
    top::check (!event.m_isLoose, "Should not have loose events here!");
    if (m_firstEvent) {
      m_firstEvent = false;
      if (m_config->isMC()) {
        int dsid = event.m_info->mcChannelNumber();
        // xsec * filtEff
        float rawXSection = m_tdpXSec.getRawXsection(dsid);
        float kFactor = m_tdpXSec.getKfactor(dsid);
        float xSection = m_tdpXSec.getXsection(dsid);
        m_metaTreeManager->makeOutputVariable(rawXSection, "RawXSection");
        m_metaTreeManager->makeOutputVariable(kFactor, "KFactor");
        m_metaTreeManager->makeOutputVariable(xSection, "XSection");
        m_metaTreeManager->fill();
      }
    }

    // We have to write the selection decisions now because they get overwritten
    // on the EventInfo each time...
    for (auto& selection :  m_selectionBranches)
      selection.second.value(event.m_hashValue) = selection.first(*event.m_info);
    // Also build up the object selection masks here
    for (const xAOD::Jet* ijet : event.m_jets)
      // This trick works because event.m_jets is a view container. View
      // containers do not track their contents' indices so the index function
      // on an element gives the index in the *original* container!
      m_jetMask.insert(ijet->index() );
    for (const xAOD::Electron* iele : event.m_electrons)
      m_eleMask.insert(iele->index() );
    //for (const xAOD::Photon* igam : event.m_photons)
    //  m_gamMask.insert(igam->index() );
    for (const xAOD::Muon* imu : event.m_muons)
    m_muMask.insert(imu->index() );
    //for (const xAOD::TauJet* itau : event.m_tauJets)
    //  m_tauMask.insert(itau->index() );
    //    for (const xAOD::MissingET* imet: event.m_met)
    // m_metMask.insert(imet->index() );
    for (const xAOD::Jet* ilrj : event.m_largeJets)
      m_lrjMask.insert(ilrj->index() );
    const xAOD::JetContainer* cont;
    top::check(evtStore()->retrieve(cont, m_config->sgKeyTrackJetsTDS(event.m_hashValue) ), "failed to retrieve container");
    for (const xAOD::Jet* itj : event.m_trackJets) {
      m_tjMask.insert(itj->index() );
    }
    const xAOD::JetContainer* tlrj = 0;
    if (m_config->isMC()) {
      top::check(evtStore()->retrieve(tlrj,"AntiKt10TruthTrimmedPtFrac5SmallR20Jets"),"Failed to retrieve AntiKt10TruthTrimmedPtFrac5SmallR20Jets");
      for (const xAOD::Jet* itlrj : *tlrj){
	m_tlrjMask.insert(itlrj->index() );
      }
    }
    xAOD::MissingETContainer* met= 0;
    top::check(evtStore()->retrieve(met, m_config->sgKeyMissingEt(event.m_hashValue) ), "Failed to retrieve MET");
    for (const xAOD::MissingET* imet: *met){
      m_metMask.insert(imet->index() );
    }
    m_events.at(event.m_ttreeIndex) = event;
  }

  void EventSaver::saveEventToxAOD()
  {
    // This is where everything *actually* happens.
    // First see if we want to save this event
    if (m_config->saveOnlySelectedEvents() && 
        std::none_of(m_events.begin(), m_events.end(),
          [] (const top::Event& event) { return event.m_saveEvent; }) ) {
      m_jetMask.clear();
      m_eleMask.clear();
      m_muMask.clear();
      //m_tauMask.clear();
      m_metMask.clear();
      m_lrjMask.clear();
      m_tlrjMask.clear();
      m_tjMask.clear();
      return;
    }

    // Build index maps
    auto jetIdxMap = constructIndexMap(m_jetMask);
    auto eleIdxMap = constructIndexMap(m_eleMask);
    auto gamIdxMap = constructIndexMap(m_gamMask);
    auto muIdxMap = constructIndexMap(m_muMask);
    //auto tauIdxMap = constructIndexMap(m_tauMask);
    auto metIdxMap = constructIndexMap(m_metMask);
    auto lrjIdxMap = constructIndexMap(m_lrjMask);
    auto tlrjIdxMap = constructIndexMap(m_tlrjMask);
    auto tjIdxMap = constructIndexMap(m_tjMask);


    // Access the JVT decision
    static SG::AuxElement::ConstAccessor<char> acc_passJVT("passJVT");
    // Get the number of output objects for each type
    std::size_t nJets = m_jetMask.size();
    std::size_t nElectrons = m_eleMask.size();
    //std::size_t nPhotons = m_gamMask.size();
    std::size_t nMuons = m_muMask.size();
    //std::size_t nTaus = m_tauMask.size();
    std::size_t nMets = m_metMask.size();
    std::size_t nLargeR = m_lrjMask.size();
    //std::size_t nTruthLargeR = m_tlrjMask.size();
    std::size_t nTrackJet = m_tjMask.size();

    
    //std::cout << "#JETS : " << nJets << std::endl;
    //std::cout << "#Electros : " << nElectrons <<std::endl;
    //std::cout << "#Muons : " << nMuons <<std::endl;
    //std::cout << "#LargeR : " << nLargeR <<std::endl;
    //std::cout << "#TruthLargeR : " << nTruthLargeR <<std::endl;
    //std::cout << "#TrackJet : " << nTrackJet <<std::endl;
    

    // Now loop over the cached events.
    for (const top::Event& event : m_events) {
      std::size_t systHash = event.m_hashValue;
      if (systHash == m_config->nominalHashValue() ) {
        // Event info stuff
        if (m_config->isMC() ) {
          m_dsid.value(systHash) = event.m_info->mcChannelNumber();
          m_eventNumber.value(systHash) = event.m_info->mcEventNumber();
          m_mcEventWeight.value(systHash) = event.m_info->mcEventWeight();
	  // m_pileupWeight.value(systHash) = event.m_info->auxdataConst<float>("PileupWeight");
	  m_prwEventWeight.value(systHash) = m_sfRetriever->pileupSF(event);
          m_prwEventWeight.value(std::hash<std::string>{}(PRWWeightUp.name() ) ) =
            m_sfRetriever->pileupSF(event, 1);
          m_prwEventWeight.value(std::hash<std::string>{}(PRWWeightDown.name() ) ) =
            m_sfRetriever->pileupSF(event, -1);
        }
        else {
          m_runNumber.value(systHash) = event.m_info->runNumber();
          m_eventNumber.value(systHash) = event.m_info->eventNumber();
        }
        m_lumiBlock.value(systHash) = event.m_info->lumiBlock();
	m_averagePileup.value(systHash) = event.m_info->averageInteractionsPerCrossing();
        m_actualPileup.value(systHash) = event.m_info->actualInteractionsPerCrossing();

      }

      // Get all the containers (this is slightly inefficient as it means redoing
      // the calculation even if the selection isn't affected by the
      // systematic but it's worth it for code simplicity).
      auto selJets = getSelectedContainer<xAOD::JetContainer>(
          m_config->sgKeyJetsTDS(systHash, false), jetIdxMap);
      auto selElectrons = getSelectedContainer<xAOD::ElectronContainer>(
          m_config->sgKeyElectronsTDS(systHash), eleIdxMap);
      ///*auto selPhotons = getSelectedContainer<xAOD::PhotonContainer>(
      //    m_config->sgKeyPhotonsTDS(systHash), gamIdxMap);
      auto selMuons = getSelectedContainer<xAOD::MuonContainer>(
      m_config->sgKeyMuonsTDS(systHash), muIdxMap);
      //auto selTaus = getSelectedContainer<xAOD::TauJetContainer>(
      //							m_config->sgKeyTausTDS(systHash), tauIdxMap);
      //auto selMets = getSelectedContainer<xAOD::MissingETContainer>(
      //m_config->sgKeyMissingEt(systHash), metIdxMap);
      //MET are not view element
      const xAOD::MissingETContainer* selMets(nullptr);
      top::check(evtStore()->retrieve(selMets, m_config->sgKeyMissingEt(systHash)), "Failed to retrieve MET");
      
      auto selLRJets = getSelectedContainer<xAOD::JetContainer>(
          m_config->sgKeyLargeRJetsTDS(systHash), lrjIdxMap);
      auto selTruthLRJets = getSelectedContainer<xAOD::JetContainer>("AntiKt10TruthTrimmedPtFrac5SmallR20Jets", tlrjIdxMap);
      auto selTrackJets = getSelectedContainer<xAOD::JetContainer>(
          m_config->sgKeyTrackJetsTDS(systHash), tjIdxMap);
										
      // Does this systematic affect OR?
      // Use the jet selection branch to check.
      if (m_jetIsSelected.isAffectedBy(systHash) ) {
      
	if (m_config->isMC()) {
          m_jvtEventWeight.value(systHash) = m_sfRetriever->jvtSF(event, top::topSFSyst::nominal);
          //m_gamEventWeight.value(systHash) = m_sfRetriever->photonSF(event, top::topSFSyst::nominal);
          // also, only do the up and down variations on the nominal event
          if (systHash == m_config->nominalHashValue() ) {
            m_jvtEventWeight.value(std::hash<std::string>{}(JVTWeightUp.name())) =
              m_sfRetriever->jvtSF(event, top::topSFSyst::JVT_UP);
            m_jvtEventWeight.value(std::hash<std::string>{}(JVTWeightDown.name())) =
              m_sfRetriever->jvtSF(event, top::topSFSyst::JVT_DOWN);

            //m_gamEventWeight.value(std::hash<std::string>{}(PhotonEffIDWeightUp.name())) =
	    //m_sfRetriever->photonSF(event, top::topSFSyst::PHOTON_IDSF_UP);
            //m_gamEventWeight.value(std::hash<std::string>{}(PhotonEffIDWeightDown.name())) =
	    // m_sfRetriever->photonSF(event, top::topSFSyst::PHOTON_IDSF_DOWN);
            //m_gamEventWeight.value(std::hash<std::string>{}(PhotonEffIsoWeightUp.name())) =
	    // m_sfRetriever->photonSF(event, top::topSFSyst::PHOTON_EFF_ISO_UP);
            //m_gamEventWeight.value(std::hash<std::string>{}(PhotonEffIsoWeightDown.name())) =
	    //m_sfRetriever->photonSF(event, top::topSFSyst::PHOTON_EFF_ISO_DOWN);
            /* m_gamEventWeight.value(std::hash<std::string>{}(PhotonEffIsoWeightUp.name())) = */
            /*   m_sfRetriever->photonSF(event, top::topSFSyst::PHOTON_EFF_ISO_UP); */
            /* m_gamEventWeight.value(std::hash<std::string>{}(PhotonEffIsoWeightDown.name())) = */
            /*   m_sfRetriever->photonSF(event, top::topSFSyst::PHOTON_EFF_ISO_DOWN); */
          }
        }
        // Get the 'tight' decision
        getSelectionDecisions(
			      m_jetIsSelected.value(systHash), event.m_jets, jetIdxMap);
	getSelectionDecisions_lepton(
			      m_eleIsSelected.value(systHash), event.m_electrons, eleIdxMap);
        //getSelectionDecisions(
	//		      m_gamIsSelected.value(systHash), event.m_photons, gamIdxMap);
        getSelectionDecisions_lepton(
			      m_muIsSelected.value(systHash), event.m_muons, muIdxMap);
	//getSelectionDecisions_lepton(
	//			     m_tauIsSelected.value(systHash), event.m_tauJets, tauIdxMap);
	// Only need to write out the kinematics on one of the two
        if (m_jetHadronLabel.isActive() && m_jetHadronLabel.isAffectedBy(systHash) ) {
          auto& jetHadronLabel = m_jetHadronLabel.value(systHash);
          jetHadronLabel.reserve(nJets);
          for (const xAOD::Jet* ijet : selJets) {
            const static SG::AuxElement::ConstAccessor<int> acc("HadronConeExclTruthLabelID");
            jetHadronLabel.push_back(acc(*ijet));
          }
        }
        // Only need to write out the kinematics on one of the two
        if (m_jetPt.isAffectedBy(systHash) ) {
           // Now get the kinematic values
           auto& jetPt = m_jetPt.value(systHash);
           auto& jetEta = m_jetEta.value(systHash);
           auto& jetPhi = m_jetPhi.value(systHash);
           auto& jetMass = m_jetMass.value(systHash);
           jetPt.reserve(nJets);
           jetEta.reserve(nJets);
           jetPhi.reserve(nJets);
           jetMass.reserve(nJets);
           for (const xAOD::Jet* ijet : selJets) {
             jetPt.push_back(ijet->pt()/1000. ); //in GeV
             jetEta.push_back(ijet->eta() );
             jetPhi.push_back(ijet->phi() );
             jetMass.push_back(ijet->m()/1000. ); //in GeV
	   }
	}	  
        if (m_elePt.isAffectedBy(systHash) ) {
           auto& elePt = m_elePt.value(systHash);
           auto& eleEta = m_eleEta.value(systHash);
           auto& elePhi = m_elePhi.value(systHash);
	   auto& eleCharge = m_eleCharge.value(systHash);
           elePt.reserve(nElectrons);
           eleEta.reserve(nElectrons);
           elePhi.reserve(nElectrons);
	   eleCharge.reserve(nElectrons);
           for (const xAOD::Electron* iele : selElectrons) {
             elePt.push_back(iele->pt()/1000. );
             eleEta.push_back(iele->eta() );
             elePhi.push_back(iele->phi() );
	     eleCharge.push_back(iele->charge()) ; 
	   }
	}
	  /**
          if (m_gamPt.isAffectedBy(systHash) ) {
            auto& gamPt = m_gamPt.value(systHash);
            auto& gamEta = m_gamEta.value(systHash);
            auto& gamPhi = m_gamPhi.value(systHash);
            gamPt.reserve(nPhotons);
            gamEta.reserve(nPhotons);
            gamPhi.reserve(nPhotons);
            for (const xAOD::Photon* igam : selPhotons) {
              gamPt.push_back(igam->pt() );
              gamEta.push_back(igam->eta() );
              gamPhi.push_back(igam->phi() );
            }
	    }*/
         if (m_muPt.isAffectedBy(systHash) ) {            
	   auto& muPt = m_muPt.value(systHash);
           auto& muEta = m_muEta.value(systHash);
           auto& muPhi = m_muPhi.value(systHash);
	   auto& muType = m_muType.value(systHash);
	   auto& muQuality = m_muQuality.value(systHash);
	   auto& muCharge = m_muCharge.value(systHash);
	   auto& muEnergyLoss = m_muEnergyLoss.value(systHash);
           muPt.reserve(nMuons);
           muEta.reserve(nMuons);
           muPhi.reserve(nMuons);
	   muType.reserve(nMuons);
	   muQuality.reserve(nMuons);
	   muCharge.reserve(nMuons);
	   muEnergyLoss.reserve(nMuons);
	   static SG::AuxElement::Accessor<float> accEnergyLoss("EnergyLoss");
           for (const xAOD::Muon* imu : selMuons) {
             muPt.push_back(imu->pt()/1000. );
             muEta.push_back(imu->eta() );
             muPhi.push_back(imu->phi() );
	     muType.push_back(imu->muonType());
	     muQuality.push_back(imu->quality());
	     muCharge.push_back(imu->charge());
	     muEnergyLoss.push_back(accEnergyLoss(*imu)/1000.);
           }
	 }
	 /*if (m_tauPt.isAffectedBy(systHash) ) {
	   auto& tauMass = m_tauMass.value(systHash);
           auto& tauPt = m_tauPt.value(systHash);
           auto& tauEta = m_tauEta.value(systHash);
           auto& tauPhi = m_tauPhi.value(systHash);
           auto& tauCharge = m_tauCharge.value(systHash);
	   tauMass.reserve(nTaus);
           tauPt.reserve(nTaus);
           tauEta.reserve(nTaus);
           tauPhi.reserve(nTaus);
           tauCharge.reserve(nTaus);
           for (const xAOD::TauJet* itau : selTaus) {
	     tauMass.push_back(itau->m()/1000. );
             tauPt.push_back(itau->pt()/1000. );
             tauEta.push_back(itau->eta() );
             tauPhi.push_back(itau->phi() );
             tauCharge.push_back(itau->charge()) ;
           }
	   }*/
	 if (m_metMet.isAffectedBy(systHash) ) {
	   auto& metMet = m_metMet.value(systHash);
	   auto& metPhi = m_metPhi.value(systHash);
	   metMet.reserve(nMets);
	   metPhi.reserve(nMets);
	   for (auto imet : *selMets) {
	     metMet.push_back(imet->met()/1000.);
	     metPhi.push_back(imet->phi()/1000.);
	   }
	 }
         if (systHash == m_config->nominalHashValue() ) {
            // Do b-tagging here
           for (auto& p : m_jetIsBTagged)
             p.second.value(systHash).reserve(nJets);
           for (const xAOD::Jet* ijet : selJets)
             for (auto& p : m_jetIsBTagged)
               p.second.value(systHash).push_back(p.first(*ijet) );
	 }
      }

        if (m_lrjIsSelected.isAffectedBy(systHash) ) {
          getSelectionDecisions(
              m_lrjIsSelected.value(systHash), event.m_largeJets, lrjIdxMap);
          // For now the selection variable is affected by the same things as the
          // kinematic variables so just write them all out together (i.e. no
          // further if statement).
	  if(m_config->isMC()) {
	    static SG::AuxElement::Accessor<int> accTruthLabel("R10TruthLabel_R21Consolidated");
	    static SG::AuxElement::Accessor<float> accTruthLabelDRW("R10TruthLabel_R21Consolidated_dR_W");
	    static SG::AuxElement::Accessor<float> accTruthLabelDRZ("R10TruthLabel_R21Consolidated_dR_Z");
	    static SG::AuxElement::Accessor<float> accTruthLabelDRH("R10TruthLabel_R21Consolidated_dR_H");
	    static SG::AuxElement::Accessor<float> accTruthLabelDRTop("R10TruthLabel_R21Consolidated_dR_Top");
	    static SG::AuxElement::Accessor<int> accTruthLabelDRNB("R10TruthLabel_R21Consolidated_NB");

	    auto& lrjTruthMass = m_lrjTruthMass.value(systHash);
	    auto& lrjTruthPt = m_lrjTruthPt.value(systHash);
	    auto& lrjTruthEta = m_lrjTruthEta.value(systHash);
	    auto& lrjTruthPhi = m_lrjTruthPhi.value(systHash);
	    auto& lrjTruthLabel = m_lrjTruthLabel.value(systHash);
	    auto& lrjTruthLabel_dR_W = m_lrjTruthLabel_dR_W.value(systHash);
	    auto& lrjTruthLabel_dR_Z = m_lrjTruthLabel_dR_Z.value(systHash);
	    auto& lrjTruthLabel_dR_H = m_lrjTruthLabel_dR_H.value(systHash);
	    auto& lrjTruthLabel_dR_Top = m_lrjTruthLabel_dR_Top.value(systHash);
	    auto& lrjTruthLabel_dR_NB = m_lrjTruthLabel_dR_NB.value(systHash);

	    lrjTruthMass.reserve(nLargeR);
	    lrjTruthPt.reserve(nLargeR);
	    lrjTruthEta.reserve(nLargeR);
	    lrjTruthPhi.reserve(nLargeR);
	    lrjTruthLabel.reserve(nLargeR);
	    lrjTruthLabel_dR_W.reserve(nLargeR);
	    lrjTruthLabel_dR_Z.reserve(nLargeR);
	    lrjTruthLabel_dR_H.reserve(nLargeR);
	    lrjTruthLabel_dR_Top.reserve(nLargeR);
	    lrjTruthLabel_dR_NB.reserve(nLargeR);
	    for (const xAOD::Jet* ijet : selLRJets) {
	      lrjTruthLabel.push_back(accTruthLabel(*ijet));
	      lrjTruthLabel_dR_W.push_back(accTruthLabelDRW(*ijet));
	      lrjTruthLabel_dR_Z.push_back(accTruthLabelDRZ(*ijet));
	      lrjTruthLabel_dR_H.push_back(accTruthLabelDRH(*ijet));
	      lrjTruthLabel_dR_Top.push_back(accTruthLabelDRTop(*ijet));
	      lrjTruthLabel_dR_NB.push_back(accTruthLabelDRNB(*ijet));

	      float dRmin = 9999;
	      const xAOD::Jet* matchedtruthJet = nullptr;
	      for ( const xAOD::Jet* truthJet : selTruthLRJets ) {
		float dR = ijet->p4().DeltaR( truthJet->p4() );
		/// If m_dRTruthJet < 0, the closest truth jet is used as matched jet. Otherwise, only match if dR < m_dRTruthJet
		if ( dR < 0.75 ) {
		  if ( dR < dRmin ) {
		    dRmin = dR;
		    matchedtruthJet = truthJet;
		  }
		}
	      }
	      if(matchedtruthJet){
		lrjTruthMass.push_back(matchedtruthJet->m()/1000.);
		lrjTruthPt.push_back(matchedtruthJet->pt()/1000.);
		lrjTruthEta.push_back(matchedtruthJet->eta());
		lrjTruthPhi.push_back(matchedtruthJet->phi());
	      }else{
		lrjTruthMass.push_back(-999.9);
                lrjTruthPt.push_back(-999.9);
		lrjTruthEta.push_back(-999.9);
		lrjTruthPhi.push_back(-999.9);
	      }
	    }
	  }

          auto& lrjPt = m_lrjPt.value(systHash);
          auto& lrjEta = m_lrjEta.value(systHash);
          auto& lrjPhi = m_lrjPhi.value(systHash);
          auto& lrjMass = m_lrjMass.value(systHash);

	  auto& lrjCaloMass = m_lrjCaloMass.value(systHash);
	  auto& lrjCaloPt = m_lrjCaloPt.value(systHash);
	  auto& lrjCaloEta = m_lrjCaloEta.value(systHash);
          auto& lrjCaloPhi = m_lrjCaloPhi.value(systHash);

	  auto& lrjTAMass = m_lrjTAMass.value(systHash);
	  auto& lrjTAPt = m_lrjTAPt.value(systHash);
          auto& lrjTAEta = m_lrjTAEta.value(systHash);
          auto& lrjTAPhi = m_lrjTAPhi.value(systHash);

          auto& lrjPassWNtrk_50 = m_lrjPassWNtrk_50.value(systHash);
          auto& lrjPassWD2_50 = m_lrjPassWD2_50.value(systHash);
	  auto& lrjPassWMassHigh_50 = m_lrjPassWMassHigh_50.value(systHash);
	  auto& lrjPassWMassLow_50 = m_lrjPassWMassLow_50.value(systHash);

          auto& lrjPassZNtrk_50 = m_lrjPassZNtrk_50.value(systHash);
	  auto& lrjPassZD2_50 = m_lrjPassZD2_50.value(systHash);
	  auto& lrjPassZMassHigh_50 = m_lrjPassZMassHigh_50.value(systHash);
          auto& lrjPassZMassLow_50 = m_lrjPassZMassLow_50.value(systHash);

          auto& lrjPassWNtrk_80 = m_lrjPassWNtrk_80.value(systHash);
          auto& lrjPassWD2_80 = m_lrjPassWD2_80.value(systHash);
          auto& lrjPassWMassHigh_80 = m_lrjPassWMassHigh_80.value(systHash);
          auto& lrjPassWMassLow_80 = m_lrjPassWMassLow_80.value(systHash);

          auto& lrjPassZNtrk_80 = m_lrjPassZNtrk_80.value(systHash);
          auto& lrjPassZD2_80 = m_lrjPassZD2_80.value(systHash);
          auto& lrjPassZMassHigh_80 = m_lrjPassZMassHigh_80.value(systHash);
          auto& lrjPassZMassLow_80 = m_lrjPassZMassLow_80.value(systHash);


	  auto& lrjHbbTop = m_lrjHbbTop.value(systHash);
	  auto& lrjHbbQCD = m_lrjHbbQCD.value(systHash);
	  auto& lrjHbbHiggs = m_lrjHbbHiggs.value(systHash);

	  auto& lrjNtrk = m_lrjNtrk.value(systHash);
	  auto& lrjNtrkPt500 = m_lrjNtrkPt500.value(systHash);
	  auto& lrjECF1 = m_lrjECF1.value(systHash); 
	  auto& lrjECF2 = m_lrjECF2.value(systHash);
	  auto& lrjECF3 = m_lrjECF3.value(systHash);
	  auto& lrjD2 = m_lrjD2.value(systHash);
	  auto& lrjTau1_wta = m_lrjTau1_wta.value(systHash);
	  auto& lrjTau2_wta = m_lrjTau2_wta.value(systHash);
	  auto& lrjTau3_wta = m_lrjTau3_wta.value(systHash);
	  auto& lrjTau21 = m_lrjTau21.value(systHash);
	  auto& lrjFoxWolfram2 = m_lrjFoxWolfram2.value(systHash);
	  auto& lrjFoxWolfram1 = m_lrjFoxWolfram1.value(systHash);
	  auto& lrjFoxWolfram0 = m_lrjFoxWolfram0.value(systHash);
	  auto& lrjSplit12 = m_lrjSplit12.value(systHash);
	  auto& lrjSplit23 = m_lrjSplit23.value(systHash);
          auto& lrjQw = m_lrjQw.value(systHash);
          auto& lrjPlanarFlow = m_lrjPlanarFlow.value(systHash);
          auto& lrjAngularity = m_lrjAngularity.value(systHash);
	  auto& lrjAplanarity = m_lrjAplanarity.value(systHash);
	  auto& lrjZCut12 = m_lrjZCut12.value(systHash);
	  auto& lrjKtDR = m_lrjKtDR.value(systHash);
	  auto& lrjThrustMin = m_lrjThrustMin.value(systHash);
	  auto& lrjThrustMaj = m_lrjThrustMaj.value(systHash);
	  auto& lrjSphericity = m_lrjSphericity.value(systHash);
	  auto& lrjMu12 = m_lrjMu12.value(systHash);
          lrjPt.reserve(nLargeR);
          lrjEta.reserve(nLargeR);
          lrjPhi.reserve(nLargeR);
          lrjMass.reserve(nLargeR);

	  lrjCaloMass.reserve(nLargeR);
	  lrjCaloPt.reserve(nLargeR);
          lrjCaloEta.reserve(nLargeR);
          lrjCaloPhi.reserve(nLargeR);

	  lrjTAMass.reserve(nLargeR);
	  lrjTAPt.reserve(nLargeR);
          lrjTAEta.reserve(nLargeR);
          lrjTAPhi.reserve(nLargeR);

	  //lrjPassWNtrkD2.reserve(nLargeR);
          lrjPassWNtrk_50.reserve(nLargeR);
          lrjPassWD2_50.reserve(nLargeR);
	  lrjPassWMassHigh_50.reserve(nLargeR);
	  lrjPassWMassLow_50.reserve(nLargeR);

          //lrjPassZNtrkD2.reserve(nLargeR);
          lrjPassZNtrk_50.reserve(nLargeR);
          lrjPassZD2_50.reserve(nLargeR);
	  lrjPassZMassHigh_50.reserve(nLargeR);
	  lrjPassZMassLow_50.reserve(nLargeR);
          lrjPassWNtrk_80.reserve(nLargeR);
          lrjPassWD2_80.reserve(nLargeR);
          lrjPassWMassHigh_80.reserve(nLargeR);
          lrjPassWMassLow_80.reserve(nLargeR);

          //lrjPassZNtrkD2.reserve(nLargeR);
          lrjPassZNtrk_80.reserve(nLargeR);
          lrjPassZD2_80.reserve(nLargeR);
          lrjPassZMassHigh_80.reserve(nLargeR);
          lrjPassZMassLow_80.reserve(nLargeR);

	  lrjHbbTop.reserve(nLargeR);
	  lrjHbbQCD.reserve(nLargeR);
	  lrjHbbHiggs.reserve(nLargeR);

	  lrjNtrk.reserve(nLargeR);
	  lrjNtrkPt500.reserve(nLargeR);
	  lrjECF1.reserve(nLargeR);
	  lrjECF2.reserve(nLargeR);
	  lrjECF3.reserve(nLargeR);
          lrjD2.reserve(nLargeR);
	  lrjTau1_wta.reserve(nLargeR);
          lrjTau2_wta.reserve(nLargeR);
	  lrjTau3_wta.reserve(nLargeR);
	  lrjTau21.reserve(nLargeR);
	  lrjFoxWolfram2.reserve(nLargeR);
	  lrjFoxWolfram1.reserve(nLargeR);
	  lrjFoxWolfram0.reserve(nLargeR);
          lrjSplit12.reserve(nLargeR);
          lrjSplit23.reserve(nLargeR);
	  lrjQw.reserve(nLargeR);
          lrjPlanarFlow.reserve(nLargeR);
          lrjAngularity.reserve(nLargeR);
	  lrjAplanarity.reserve(nLargeR);
          lrjZCut12.reserve(nLargeR);
          lrjKtDR.reserve(nLargeR);
	  lrjThrustMin.reserve(nLargeR);
	  lrjThrustMaj.reserve(nLargeR);
	  lrjSphericity.reserve(nLargeR);
	  lrjMu12.reserve(nLargeR);

          for (const xAOD::Jet* ijet : selLRJets) {
            lrjPt.push_back(ijet->pt()/1000. );
            lrjEta.push_back(ijet->eta() );
            lrjPhi.push_back(ijet->phi() );
            lrjMass.push_back(ijet->m()/1000. );

	    std::string m_calibratedMassDecorator;
	    m_calibratedMassDecorator = m_config->isMC()?"JetJMSScaleMomentum":"JetInsituScaleMomentum";
	    static SG::AuxElement::Accessor<float> accCaloMass(m_calibratedMassDecorator+"Calo_m");
	    static SG::AuxElement::Accessor<float> accCaloPt(m_calibratedMassDecorator+"Calo_pt");
	    static SG::AuxElement::Accessor<float> accCaloEta(m_calibratedMassDecorator+"Calo_eta");
	    static SG::AuxElement::Accessor<float> accCaloPhi(m_calibratedMassDecorator+"Calo_phi");
	    static SG::AuxElement::Accessor<float> accTAMass(m_calibratedMassDecorator+"TA_m");
	    static SG::AuxElement::Accessor<float> accTAPt(m_calibratedMassDecorator+"TA_pt");
	    static SG::AuxElement::Accessor<float> accTAEta(m_calibratedMassDecorator+"TA_eta");
	    static SG::AuxElement::Accessor<float> accTAPhi(m_calibratedMassDecorator+"TA_phi");

	    lrjCaloMass.push_back(accCaloMass(*ijet)/1000. );
	    lrjCaloPt.push_back(accCaloPt(*ijet)/1000. );
	    lrjCaloEta.push_back(accCaloEta(*ijet));
	    lrjCaloPhi.push_back(accCaloPhi(*ijet));
	    lrjTAMass.push_back(accTAMass(*ijet)/1000. );
	    lrjTAPt.push_back(accTAPt(*ijet)/1000. );
            lrjTAEta.push_back(accTAEta(*ijet));
            lrjTAPhi.push_back(accTAPhi(*ijet));

	    //ungroomed track information
	    static SG::AuxElement::Accessor<ElementLink<xAOD::JetContainer> > ungroomedLink("Parent");
	    const xAOD::Jet * ungroomedJet = 0;
	    if( ungroomedLink.isAvailable( *ijet ) ) {
	      ElementLink<xAOD::JetContainer> linkToUngroomed = ungroomedLink( *ijet );
	      if (  linkToUngroomed.isValid() ) ungroomedJet = *linkToUngroomed;
	      std::vector<int> NTrkPt500;
	      ungroomedJet->getAttribute(xAOD::JetAttribute::NumTrkPt500, NTrkPt500);
	      if (NTrkPt500.size()==0) {
		lrjNtrkPt500.push_back(-99.9);
	      } else {
		lrjNtrkPt500.push_back(NTrkPt500.at(0));
	      }
	    }

	    //W/Z tagger for LCTopo trimmed jet
            unsigned long resultW_80 = ijet->auxdata<unsigned long>("TAccept_bitmap_SmoothedWZTagger_SmoothWContained80");
	    unsigned long resultZ_80 = ijet->auxdata<unsigned long>("TAccept_bitmap_SmoothedWZTagger_SmoothZContained80");
            unsigned long resultW_50 = ijet->auxdata<unsigned long>("TAccept_bitmap_SmoothedWZTagger_SmoothWContained50");
            unsigned long resultZ_50 = ijet->auxdata<unsigned long>("TAccept_bitmap_SmoothedWZTagger_SmoothZContained50");
            //cut0: "ValidPtRangeHigh"    , "True if the jet is not too high pT"
	    //cut1: "ValidPtRangeLow"     , "True if the jet is not too low pT"
	    //cut2: "ValidEtaRange"       , "True if the jet is not too forward"
	    //cut3: "ValidJetContent"     , "True if the jet is alright technicall (e.g. all attributes necessary for tag)"
	    //cut4: "PassMassLow"         , "mJet > mCutLow"
	    //cut5: "PassMassHigh"        , "mJet < mCutHigh"
	    //cut6: "PassD2"              , "D2Jet < D2Cut"
	    //cut7:  "ValidEventContent" , "True if primary vertex was found"
	    //cut8: "PassNtrk"          , "NtrkJet < NtrkCut" 
            //const Root::TAccept& res = m_smoothedWTagger->tag(*ijet);
            //bool isW = res.getCutResult("PassD2")&&res.getCutResult("PassMassLow")&&res.getCutResult("PassMassHigh")&&res.getCutResult("PassNtrk");
            //bool isW = ( ((result >> 9) & 1) && ((result >> 5) & 1) && ((result >> 6) & 1) && ((result >> 7) & 1) );
            //lrjIsW.push_back( isW );
	    //lrjPassWNtrkD2.push_back( ((resultW>> 6) & 1) && ((resultW >> 8) & 1) );
	    lrjPassWNtrk_50.push_back( (resultW_50 >> 8) & 1 );
            lrjPassWD2_50.push_back( (resultW_50 >> 6) & 1 );
	    lrjPassWMassHigh_50.push_back( (resultW_50 >> 5) & 1);
	    lrjPassWMassLow_50.push_back( (resultW_50 >> 4) & 1);
            //unsigned long resultZ = ijet->auxdata<unsigned long>("TAccept_bitmap_SmoothedWZTagger_SmoothWContained80");
	    //lrjPassZNtrkD2.push_back( ((resultZ>> 7) & 1) && ((resultZ >> 9) & 1) );
            lrjPassZNtrk_50.push_back( (resultZ_50 >> 8) & 1 );
	    lrjPassZD2_50.push_back( (resultZ_50 >> 6) & 1 );
	    lrjPassZMassHigh_50.push_back( (resultZ_50 >> 5) & 1);
	    lrjPassZMassLow_50.push_back( (resultZ_50 >> 4) & 1);

            lrjPassWNtrk_80.push_back( (resultW_80 >> 8) & 1 );
            lrjPassWD2_80.push_back( (resultW_80 >> 6) & 1 );
            lrjPassWMassHigh_80.push_back( (resultW_80 >> 5) & 1);
            lrjPassWMassLow_80.push_back( (resultW_80 >> 4) & 1);
            //unsigned long resultZ = ijet->auxdata<unsigned long>("TAccept_bitmap_SmoothedWZTagger_SmoothWContained80");
            //lrjPassZNtrkD2.push_back( ((resultZ>> 7) & 1) && ((resultZ >> 9) & 1) );
            lrjPassZNtrk_80.push_back( (resultZ_80 >> 8) & 1 );
            lrjPassZD2_80.push_back( (resultZ_80 >> 6) & 1 );
            lrjPassZMassHigh_80.push_back( (resultZ_80 >> 5) & 1);
            lrjPassZMassLow_80.push_back( (resultZ_80 >> 4) & 1);

	    lrjNtrk.push_back(ijet->getAttribute<int>("GhostTrackCount"));
	    static SG::AuxElement::Accessor<float> accECF1("ECF1");
	    static SG::AuxElement::Accessor<float> accECF2("ECF2");
	    static SG::AuxElement::Accessor<float> accECF3("ECF3");
	    lrjECF1.push_back(accECF1(*ijet));
	    lrjECF2.push_back(accECF2(*ijet));
	    lrjECF3.push_back(accECF3(*ijet));
	    lrjD2.push_back(accECF3(*ijet)*pow(accECF1(*ijet),3.0)/pow(accECF2(*ijet),3.0));

	        
	    static SG::AuxElement::Accessor<float> accTau1_wta("Tau1_wta");
	    static SG::AuxElement::Accessor<float> accTau2_wta("Tau2_wta");
	    static SG::AuxElement::Accessor<float> accTau3_wta("Tau3_wta");
	    static SG::AuxElement::Accessor<float> accFoxWolfram2("FoxWolfram2");
	    static SG::AuxElement::Accessor<float> accFoxWolfram1("FoxWolfram1");
	    static SG::AuxElement::Accessor<float> accFoxWolfram0("FoxWolfram0");
	    static SG::AuxElement::Accessor<float> accSplit12("Split12");
	    static SG::AuxElement::Accessor<float> accSplit23("Split23");
	    static SG::AuxElement::Accessor<float> accQw("Qw");
	    static SG::AuxElement::Accessor<float> accPlanarFlow("PlanarFlow");
	    static SG::AuxElement::Accessor<float> accAngularity("Angularity");
	    static SG::AuxElement::Accessor<float> accAplanarity("Aplanarity");
	    static SG::AuxElement::Accessor<float> accZCut12("ZCut12");
	    static SG::AuxElement::Accessor<float> accKtDR("KtDR");
	    static SG::AuxElement::Accessor<float> accThrustMin("ThrustMin");
	    static SG::AuxElement::Accessor<float> accThrustMaj("ThrustMaj");
	    static SG::AuxElement::Accessor<float> accSphericity("Sphericity");
	    static SG::AuxElement::Accessor<float> accMu12("Mu12");

	    lrjTau1_wta.push_back(accTau1_wta(*ijet));
	    lrjTau2_wta.push_back(accTau2_wta(*ijet));
	    lrjTau3_wta.push_back(accTau3_wta(*ijet));
	    lrjTau21.push_back(accTau2_wta(*ijet)/accTau1_wta(*ijet));
	    lrjFoxWolfram2.push_back(accFoxWolfram2(*ijet));
	    lrjFoxWolfram1.push_back(accFoxWolfram1(*ijet));
	    lrjFoxWolfram0.push_back(accFoxWolfram0(*ijet));
	    lrjSplit12.push_back(accSplit12(*ijet));
	    lrjSplit23.push_back(accSplit23(*ijet));
	    lrjQw.push_back(accQw(*ijet));
	    lrjPlanarFlow.push_back(accPlanarFlow(*ijet));
	    lrjAngularity.push_back(accAngularity(*ijet));
	    lrjAplanarity.push_back(accAplanarity(*ijet));
	    lrjZCut12.push_back(accZCut12(*ijet));
	    lrjKtDR.push_back(accKtDR(*ijet));
	    lrjThrustMin.push_back(accThrustMin(*ijet));
	    lrjThrustMaj.push_back(accThrustMaj(*ijet));
	    lrjSphericity.push_back(accSphericity(*ijet));
	    lrjMu12.push_back(accMu12(*ijet));

	    m_hbbTag->decorate(*ijet);
	    const static SG::AuxElement::ConstAccessor<float> accHbbHiggs("SubjetBScore_Higgs");
	    const static SG::AuxElement::ConstAccessor<float> accHbbQCD("SubjetBScore_QCD");
	    const static SG::AuxElement::ConstAccessor<float> accHbbTop("SubjetBScore_Top");
	    lrjHbbTop.push_back(accHbbTop(*ijet));
	    lrjHbbQCD.push_back(accHbbQCD(*ijet));
	    lrjHbbHiggs.push_back(accHbbHiggs(*ijet));	  
	  }
	 
          if (systHash == m_config->nominalHashValue() ) {
            // track-jet association is only on the nominal branch.
            auto& lrjGATrackJets = m_lrjAssociatedTrackJets.value(systHash);
            lrjGATrackJets.reserve(nLargeR);
            // We don't have systematic variations for Xbb either
            auto& xbbTop = m_lrjXbbTop.value(systHash);
            xbbTop.reserve(nLargeR);
            auto& xbbQCD = m_lrjXbbQCD.value(systHash);
            xbbQCD.reserve(nLargeR);
            auto& xbbHiggs = m_lrjXbbHiggs.value(systHash);
            xbbHiggs.reserve(nLargeR);
            // Also get the track jets here
            for (const xAOD::Jet* ijet : selLRJets) {
              lrjGATrackJets.emplace_back();
              auto& tjIndices = lrjGATrackJets.back();
              for (const auto& tjLink : accTrackJets(**accParent(*ijet)) ) {
                // For now skip any invalid links. I'm not sure if this should
                // actually trigger a job failure - input would be welcome if
                // anyone knows.
                if (!tjLink.isValid() ) {
                  ATH_MSG_WARNING("Invalid track link found!");
                  continue;
                }
                // Rely on the fact that the input containers are all shallow
                // copies of each other so the input->output index mapping is
                // always valid
                auto itr = tjIdxMap.left.find(tjLink.index() );
                if (itr != tjIdxMap.left.end() )
                  tjIndices.push_back(itr->second);
              }
              static SG::AuxElement::ConstAccessor<float> accXbbTop("XbbScoreTop");
              static SG::AuxElement::ConstAccessor<float> accXbbQCD("XbbScoreQCD");
              static SG::AuxElement::ConstAccessor<float> accXbbHiggs("XbbScoreHiggs");
              xbbTop.push_back(accXbbTop(*ijet) );
              xbbQCD.push_back(accXbbQCD(*ijet) );
              xbbHiggs.push_back(accXbbHiggs(*ijet) );
            }
          } //> end if syst is nominal
	} //> end if syst affects Large jet selection


        if (systHash == m_config->nominalHashValue() ) {
          getSelectionDecisions(
              m_tjetIsSelected.value(systHash), event.m_trackJets, tjIdxMap);
	  if (m_tjetHadronLabel.isActive() ) {
	    auto& tjetHadronLabel = m_tjetHadronLabel.value(systHash);
	    tjetHadronLabel.reserve(nTrackJet);
	    const static SG::AuxElement::ConstAccessor<int> acc("HadronConeExclTruthLabelID");
	    for (const xAOD::Jet* itjet : selTrackJets)
	      tjetHadronLabel.push_back(acc(*itjet));
	  }

          auto& tjPt = m_tjetPt.value(systHash);
          auto& tjEta = m_tjetEta.value(systHash);
          auto& tjPhi = m_tjetPhi.value(systHash);
          auto& tjMass = m_tjetMass.value(systHash);
          auto& tjPassDRCut = m_tjetPassDRCut.value(systHash);
	  auto& tjDL1pb = m_tjetDL1pb.value(systHash);
	  auto& tjDL1pc = m_tjetDL1pc.value(systHash);
	  auto& tjDL1pu = m_tjetDL1pu.value(systHash);
	  auto& tjDL1rpb = m_tjetDL1rpb.value(systHash);
	  auto& tjDL1rpc = m_tjetDL1rpc.value(systHash);
	  auto& tjDL1rpu = m_tjetDL1rpu.value(systHash);
	  auto& tjDL1rmupb = m_tjetDL1rmupb.value(systHash);
	  auto& tjDL1rmupc = m_tjetDL1rmupc.value(systHash);
	  auto& tjDL1rmupu = m_tjetDL1rmupu.value(systHash);

          static SG::AuxElement::ConstAccessor<char> accPassDR("passDRcut");
          tjPt.reserve(nTrackJet);
          tjEta.reserve(nTrackJet);
          tjPhi.reserve(nTrackJet);
          tjMass.reserve(nTrackJet);
          tjPassDRCut.reserve(nTrackJet);
	  tjDL1pb.reserve(nTrackJet);
	  tjDL1pc.reserve(nTrackJet);
	  tjDL1pu.reserve(nTrackJet);
	  tjDL1rpb.reserve(nTrackJet);
	  tjDL1rpc.reserve(nTrackJet);
	  tjDL1rpu.reserve(nTrackJet);
	  tjDL1rmupb.reserve(nTrackJet);
	  tjDL1rmupc.reserve(nTrackJet);
	  tjDL1rmupu.reserve(nTrackJet);

	  //comment b-tagging part
          for (auto& p : m_trackJetIsBTagged)
            p.second.value(systHash).reserve(nTrackJet);

          for (const xAOD::Jet* itjet : selTrackJets) {
            tjPt.push_back(itjet->pt()/1000. );
            tjEta.push_back(itjet->eta() );
            tjPhi.push_back(itjet->phi() );
            tjMass.push_back(itjet->m()/1000. );
            tjPassDRCut.push_back(accPassDR(*itjet) );
	    //comment b-tagging part
            for (auto& p : m_trackJetIsBTagged)
	      p.second.value(systHash).push_back(p.first->accept(*itjet) );
	    //p.second.value(systHash).push_back(p.first(*itjet) );
	    double dummy = -1;
	    tjDL1pb.push_back(dummy);
	    top::check(itjet->btagging()->pc("DL1", dummy), "Failed to retrieve DL1pc");
	    tjDL1pc.push_back(dummy);
	    top::check(itjet->btagging()->pu("DL1", dummy), "Failed to retrieve DL1pu");
	    tjDL1pu.push_back(dummy);
	    top::check(itjet->btagging()->pb("DL1r", dummy), "Failed to retrieve DL1rpb");
	    tjDL1rpb.push_back(dummy);
	    top::check(itjet->btagging()->pc("DL1r", dummy), "Failed to retrieve DL1rpc");
	    tjDL1rpc.push_back(dummy);
	    top::check(itjet->btagging()->pu("DL1r", dummy), "Failed to retrieve DL1rpu");
	    tjDL1rpu.push_back(dummy);
	    top::check(itjet->btagging()->pb("DL1rmu", dummy), "Failed to retrieve DL1rmupb");
	    tjDL1rmupb.push_back(dummy);
	    top::check(itjet->btagging()->pc("DL1rmu", dummy), "Failed to retrieve DL1rmupc");
	    tjDL1rmupc.push_back(dummy);
	    top::check(itjet->btagging()->pu("DL1rmu", dummy), "Failed to retrieve DL1rmupu");
	    tjDL1rmupu.push_back(dummy);
	    
	  }
	}
	
	for (auto& tree : m_treeManagers)
	  tree->fill();
	// Reset the branches. Might even be an idea to build a bool into these
	// objects that we can check every time we call fill to make sure that the
	// branches were actually set each event before fill was called?
	for (BranchBase* b : m_allBranches)
	  b->reset();
	m_jetMask.clear();
	m_eleMask.clear();
	//m_gamMask.clear();
	m_muMask.clear();
	m_lrjMask.clear();
	m_tlrjMask.clear();
	m_tjMask.clear();
    }
  }

  void EventSaver::finalize()
  {
    m_fOut->Write();
  }

  template <typename T>
    ConstDataVector<T> EventSaver::getSelectedContainer(
        const std::string& key,
        const boost::bimap<std::size_t, std::size_t>& indexMap)
    {
      ConstDataVector<T> selected(indexMap.size(), SG::VIEW_ELEMENTS);
      const T* unselected(nullptr);
      top::check( evtStore()->retrieve(unselected, key), 
          "Failed to retrieve container " + key);
      //ATH_MSG_INFO("Container size is " << unselected->size() );
      for (const auto& idxPair : indexMap)
        selected.at(idxPair.right) = unselected->at(idxPair.left);
      return selected;
    }

  template <typename T>
    void EventSaver::getSelectionDecisions(
        std::vector<char>& decisions,
        const DataVector<T>& selObjects,
        const boost::bimap<std::size_t, std::size_t>& indexMap)
    {
      // Start off with everything false
      decisions = std::vector<char>(indexMap.size(), false);
      // This also relies on the fact that selObjects is a view container
      for (const T* sel : selObjects)
        // The left map goes from input index to output index
        decisions.at(indexMap.left.at(sel->index() ) ) = true;
    }

    template <typename T>
    void EventSaver::getSelectionDecisions_lepton(
					   std::vector<char>& decisions,
					   const DataVector<T>& selObjects,
					   const boost::bimap<std::size_t, std::size_t>& indexMap)
    {
      // Start off with everything false
      decisions = std::vector<char>(indexMap.size(), true);
      // This also relies on the fact that selObjects is a view container
      for (const T* sel : selObjects)
        // The left map goes from input index to output index
        decisions.at(indexMap.left.at(sel->index() ) ) = true;
    }

  EventSaver::BranchBase::BranchBase(
      EventSaver* parent,
      const std::string& name,
      const std::regex& systRegex,
      std::function<bool(const top::TopConfig&)> activateIf) :
    m_parent(parent),
    m_name(name),
    m_systRegex(systRegex),
    m_activateIf(activateIf)
  {
    registerThis();
  }

  EventSaver::BranchBase::BranchBase(BranchBase&& other) :
    m_parent(other.m_parent),
    m_name(std::move(other.m_name) ),
    m_systRegex(std::move(other.m_systRegex) ),
    m_activateIf(std::move(other.m_activateIf) ),
    m_systs(std::move(other.m_systs) )
  {
    // If the other was in a TTree then this is an error. It means that the
    // TTree will try reading from dead memory in a fill statement. Best case
    // scenario here is a segfault - worst case is that the program keeps
    // going!
    if (other.m_isInTree)
      throw std::runtime_error("Move called on branch " + m_name + " after "
          "it has already been added to a tree!");
    other.deregisterThis();
    registerThis();
  }


  void EventSaver::BranchBase::provideSystematics(
      const std::set<std::size_t>& systs)
  {
    top::check(m_systs.empty(), "provideSystematics called on a branch that "
        "already has systematics loaded!");
    m_systs = systs;
  }

    void EventSaver::BranchBase::compileSystematics(
						    std::size_t nominalHash,
						    const std::vector<std::pair<std::size_t, std::string>>& allSysts)
    {
      if (!m_systs.empty() )
	// Something has already provided these
	return;
      // Everything affects nominal :)
      m_systs.insert(nominalHash);
      // This is really inefficient but it only happens once per branch per run so
      // unless it proves to be a significant slow-down I'm not changing it :)
      for (const auto& p : allSysts) {
	if (std::regex_match(p.second, m_systRegex) )
	  m_systs.insert(p.first);
      }
    }


  bool EventSaver::BranchBase::isActive()
  {
    // If the parent object hasn't loaded its configuration yet then return
    // false.
    if (!m_parent->m_config)
      return false;
    return m_activateIf(*(m_parent->m_config) );
  }

  void EventSaver::BranchBase::registerThis()
  {
    // Add it into the allBranches vector if it isn't already there
    if (std::find(
          m_parent->m_allBranches.begin(),
          m_parent->m_allBranches.end(),
          this) == m_parent->m_allBranches.end() )
      m_parent->m_allBranches.push_back(this);
  }

  void EventSaver::BranchBase::deregisterThis()
  {
    m_parent->m_allBranches.erase(std::remove(
          m_parent->m_allBranches.begin(),
          m_parent->m_allBranches.end(),
          this), m_parent->m_allBranches.end() );
  }

  template <typename T>
    T& EventSaver::Branch<T>::value(std::size_t systHash)
    {
      auto itr = m_values.find(systHash);
      top::check(itr != m_values.end(),
          name() + " does not have a value for variation " + 
		 m_parent->m_config->systematicName(systHash) + "with hash " + std::to_string(systHash) );
      return itr->second;
    }
  } /*> end namespace HCZC */ } /*> end namespace top */
