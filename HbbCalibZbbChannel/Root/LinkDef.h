#ifndef HBBCALIBZBBCHANNEL_LINKDEF_H
#define HBBCALIBZBBCHANNEL_LINKDEF_H

#include "HbbCalibZbbChannel/EventSaver.h"

// Some common definitions:
#pragma extra_include "HbbCalibZbbChannel/EventSaver.h";
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

// Load our custom classes into a ROOT dictionary
#pragma link C++ class top::HCZC::EventSaver+;
#endif 
