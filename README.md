**Setup atlas and athena**

```
source setupATLAS`

git clone https://gitlab.cern.ch/yajun/vzjetssf-atopntuple.git

cd vzjetssf-atopntuple
```
**Checkout athena, and switch to the 21.2 analysis branch**
```
lsetup git

git atlas init-workdir -b 21.2 https://:@gitlab.cern.ch:8443/atlas/athena.git

cp -r ../HbbCalibZbbChannel ./athena/
cp -r ../HowtoExtendAnalysisTop ./athena/
```

**Compile**
```
source setup.sh
source compile.sh
```

**Run locally**
```
cd run
top-xaod CUTFILE INPUTFILE
```
for example: `top-xaod cutfileforTest.txt inputforTest-zmm-sherpa221.txt`

**Run on Grid**
```
cd run/grid
./01SubmitToGrid.py
```