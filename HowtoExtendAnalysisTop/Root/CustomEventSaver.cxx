/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "HowtoExtendAnalysisTop/CustomEventSaver.h"
#include "TopEvent/Event.h"
#include "TopEvent/EventTools.h"
#include "TopConfiguration/TopConfig.h"
#include "TopEventSelectionTools/TreeManager.h"
// FTag includes
#include "xAODBTaggingEfficiency/BTaggingSelectionTool.h"
#include "FlavorTagDiscriminants/HbbTagConfig.h"// for hbbtag
#include "FlavorTagDiscriminants/HbbTag.h"
#include "PATCore/TAccept.h"
#include "lwtnn/LightweightGraph.hh"

#include <TRandom3.h>

namespace {
  static SG::AuxElement::ConstAccessor<
    std::vector<ElementLink<xAOD::IParticleContainer>>
    > accTrackJets("GhostVR30Rmax4Rmin02TrackJet_BTagging201903");
  static SG::AuxElement::ConstAccessor<ElementLink<xAOD::JetContainer>>
  accParent("Parent");
  boost::bimap<std::size_t, std::size_t> constructIndexMap(const std::set<std::size_t>& s)
  {
    using bm_type = boost::bimap<std::size_t, std::size_t>;
    bm_type ret;
    std::size_t outIdx = 0;
    auto itr = s.begin();
    for (; itr != s.end(); ++itr, ++outIdx)
      ret.insert(bm_type::value_type(*itr, outIdx) );
    return ret;
  }
}

namespace top{
  ///-- Constrcutor --///
  CustomEventSaver::CustomEventSaver() :
    m_randomNumber(0.),
    m_someOtherVariable(0.)
  {
  }
  
  ///-- initialize - done once at the start of a job before the loop over events --///
  void CustomEventSaver::initialize(std::shared_ptr<top::TopConfig> config, TFile* file, const std::vector<std::string>& extraBranches)
  {
    ///-- Let the base class do all the hard work --///
    ///-- It will setup TTrees for each systematic with a standard set of variables --///
    top::EventSaverFlatNtuple::initialize(config, file, extraBranches);
    struct FlavorTagDiscriminants::HbbTagConfig hbbConfig("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/BTagging/202002/GhostVR30Rmax4Rmin02TrackJet_BTagging201903/network.json");
    m_hbbTag = std::make_unique<FlavorTagDiscriminants::HbbTag>(hbbConfig);
    struct FlavorTagDiscriminants::HbbTagConfig hbbConfigV2("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/BTagging/2020v2/Xbb/GhostVR30Rmax4Rmin02TrackJet_BTagging201903/network.json");
    m_hbbTagV2 = std::make_unique<FlavorTagDiscriminants::HbbTag>(hbbConfigV2);

    ///-- Loop over the systematic TTrees and add the custom variables --///
    for (auto systematicTree : treeManagers()) {
      if(systematicTree->name() == "nominal"){
	systematicTree->makeOutputVariable(m_mu_type,"mu_type");
	systematicTree->makeOutputVariable(m_mu_quality,"mu_quality");
	systematicTree->makeOutputVariable(m_ljet_AssociatedTrackJets,"ljet_AssociatedTrackJets");
	systematicTree->makeOutputVariable(m_tjet_HadronLabel,"tjet_HadronLabel");
      }
      systematicTree->makeOutputVariable(m_mu_eloss,"mu_eloss");
      systematicTree->makeOutputVariable(m_ljet_calomass,"ljet_calomass");
      systematicTree->makeOutputVariable(m_ljet_calopt,"ljet_calopt");
      systematicTree->makeOutputVariable(m_ljet_caloeta,"ljet_caloeta");
      systematicTree->makeOutputVariable(m_ljet_calophi,"ljet_calophi");
      systematicTree->makeOutputVariable(m_ljet_TAmass,"ljet_TAmass");
      systematicTree->makeOutputVariable(m_ljet_TApt,"ljet_TApt");
      systematicTree->makeOutputVariable(m_ljet_TAeta,"ljet_TAeta");
      systematicTree->makeOutputVariable(m_ljet_TAphi,"ljet_TAphi");
      if(topConfig()->isMC()){
	systematicTree->makeOutputVariable(m_ljet_truthmass,"ljet_truthmass");
	systematicTree->makeOutputVariable(m_ljet_truthpt,"ljet_truthpt");
	systematicTree->makeOutputVariable(m_ljet_trutheta,"ljet_trutheta");
	systematicTree->makeOutputVariable(m_ljet_truthphi,"ljet_truthphi");
      }
      systematicTree->makeOutputVariable(m_ljet_HbbTop,"ljet_HbbTop");
      systematicTree->makeOutputVariable(m_ljet_HbbQCD,"ljet_HbbQCD");
      systematicTree->makeOutputVariable(m_ljet_HbbHiggs,"ljet_HbbHiggs");

      systematicTree->makeOutputVariable(m_ljet_HbbTopV2,"ljet_HbbTopV2");
      systematicTree->makeOutputVariable(m_ljet_HbbQCDV2,"ljet_HbbQCDV2");
      systematicTree->makeOutputVariable(m_ljet_HbbHiggsV2,"ljet_HbbHiggsV2");

      systematicTree->makeOutputVariable(m_ljet_Ntrk,"ljet_Ntrk");
      
      systematicTree->makeOutputVariable(m_ljet_PassWNtrk_50,"ljet_PassWNtrk_50");
      systematicTree->makeOutputVariable(m_ljet_PassWD2_50,"ljet_PassWD2_50");
      systematicTree->makeOutputVariable(m_ljet_PassWMassLow_50,"ljet_PassWMassLow_50");
      systematicTree->makeOutputVariable(m_ljet_PassWMassHigh_50,"ljet_PassWMassHigh_50");
      systematicTree->makeOutputVariable(m_ljet_PassWNtrk_80,"ljet_PassWNtrk_80");
      systematicTree->makeOutputVariable(m_ljet_PassWD2_80,"ljet_PassWD2_80");
      systematicTree->makeOutputVariable(m_ljet_PassWMassLow_80,"ljet_PassWMassLow_80");
      systematicTree->makeOutputVariable(m_ljet_PassWMassHigh_80,"ljet_PassWMassHigh_80");

      systematicTree->makeOutputVariable(m_ljet_PassZNtrk_50,"ljet_PassZNtrk_50");
      systematicTree->makeOutputVariable(m_ljet_PassZD2_50,"ljet_PassZD2_50");
      systematicTree->makeOutputVariable(m_ljet_PassZMassLow_50,"ljet_PassZMassLow_50");
      systematicTree->makeOutputVariable(m_ljet_PassZMassHigh_50,"ljet_PassZMassHigh_50");
      systematicTree->makeOutputVariable(m_ljet_PassZNtrk_80,"ljet_PassZNtrk_80");
      systematicTree->makeOutputVariable(m_ljet_PassZD2_80,"ljet_PassZD2_80");
      systematicTree->makeOutputVariable(m_ljet_PassZMassLow_80,"ljet_PassZMassLow_80");
      systematicTree->makeOutputVariable(m_ljet_PassZMassHigh_80,"ljet_PassZMassHigh_80");
 
      systematicTree->makeOutputVariable(m_ljet_ungrommedNtrk,"ljet_ungrommedNtrk");   
      
    }
  }
  
  ///-- saveEvent - run for every systematic and every event --///
  void CustomEventSaver::saveEvent(const top::Event& event) 
  {
    //event.m_hashValue == m_config->nominalHashValue()

    if (topConfig()->useMuons()) {
      const unsigned int nMuons = event.m_muons.size();
      if (event.m_hashValue == topConfig()->nominalHashValue()) {
	m_mu_type.resize(nMuons);
	m_mu_quality.resize(nMuons);
      }
      m_mu_eloss.resize(nMuons);
      for (const xAOD::Muon* imu : event.m_muons) {
	if (event.m_hashValue == topConfig()->nominalHashValue()) {
	  m_mu_type.push_back(imu->muonType());
	  m_mu_quality.push_back(imu->quality());
	}
	static SG::AuxElement::Accessor<float> accEnergyLoss("EnergyLoss");
	m_mu_eloss.push_back(accEnergyLoss(*imu)/1000.);
      }
    }

    if (topConfig()->isMC() && topConfig()->useTrackJets()) {
      if (event.m_hashValue == topConfig()->nominalHashValue()) {
	const unsigned int nTrkJets = event.m_trackJets.size();
	m_tjet_HadronLabel.resize(nTrkJets);
	for (const auto* const itjet : event.m_trackJets) {
	  const static SG::AuxElement::ConstAccessor<int> acc("HadronConeExclTruthLabelID");
          m_tjet_HadronLabel.push_back(acc(*itjet));
	}
      }
    }

    if (topConfig()->useLargeRJets()) {
      const unsigned int nLargeRJets = event.m_largeJets.size();

      if(topConfig()->isMC()){
	const xAOD::JetContainer* tlrj = 0;
	top::check(evtStore()->retrieve(tlrj,topConfig()->sgKeyTruthLargeRJets()),"Failed to retrieve truth large-R jet:" + topConfig()->sgKeyTruthLargeRJets());
	m_ljet_truthmass.resize(nLargeRJets);
	m_ljet_truthpt.resize(nLargeRJets);
	m_ljet_trutheta.resize(nLargeRJets);
	m_ljet_truthphi.resize(nLargeRJets);
	for (const xAOD::Jet* ijet : event.m_largeJets) {

	  float dRmin = 9999;
	  const xAOD::Jet* matchedtruthJet = nullptr;
	  for ( const xAOD::Jet* truthJet : *tlrj ) {
	    float dR = ijet->p4().DeltaR( truthJet->p4() );
	    /// If m_dRTruthJet < 0, the closest truth jet is used as matched jet. Otherwise, only match if dR < m_dRTruthJet
	    if ( dR < 0.75 ) {
	      if ( dR < dRmin ) {
		dRmin = dR;
		matchedtruthJet = truthJet;
	      }
	    }
	  }
	  if(matchedtruthJet){
	    m_ljet_truthmass.push_back(matchedtruthJet->m()/1000.);
	    m_ljet_truthpt.push_back(matchedtruthJet->pt()/1000.);
	    m_ljet_trutheta.push_back(matchedtruthJet->eta());
	    m_ljet_truthphi.push_back(matchedtruthJet->phi());
	  }else{
	    m_ljet_truthmass.push_back(-999.9);
	    m_ljet_truthpt.push_back(-999.9);
	    m_ljet_trutheta.push_back(-999.9);
	    m_ljet_trutheta.push_back(-999.9);
	  }
	}
      }
      m_ljet_calomass.resize(nLargeRJets);
      m_ljet_calopt.resize(nLargeRJets);
      m_ljet_caloeta.resize(nLargeRJets);
      m_ljet_calophi.resize(nLargeRJets);

      m_ljet_TAmass.resize(nLargeRJets);
      m_ljet_TApt.resize(nLargeRJets);
      m_ljet_TAeta.resize(nLargeRJets);
      m_ljet_TAphi.resize(nLargeRJets);

      m_ljet_HbbTop.resize(nLargeRJets);      
      m_ljet_HbbQCD.resize(nLargeRJets);
      m_ljet_HbbHiggs.resize(nLargeRJets);

      m_ljet_Ntrk.resize(nLargeRJets);

      m_ljet_PassWNtrk_50.resize(nLargeRJets);
      m_ljet_PassWD2_50.resize(nLargeRJets);
      m_ljet_PassWMassLow_50.resize(nLargeRJets);
      m_ljet_PassWMassHigh_50.resize(nLargeRJets);
      m_ljet_PassWNtrk_80.resize(nLargeRJets);
      m_ljet_PassWD2_80.resize(nLargeRJets);
      m_ljet_PassWMassLow_80.resize(nLargeRJets);
      m_ljet_PassWMassHigh_80.resize(nLargeRJets);
      m_ljet_PassZNtrk_50.resize(nLargeRJets);
      m_ljet_PassZD2_50.resize(nLargeRJets);
      m_ljet_PassZMassLow_50.resize(nLargeRJets);
      m_ljet_PassZMassHigh_50.resize(nLargeRJets);
      m_ljet_PassZNtrk_80.resize(nLargeRJets);
      m_ljet_PassZD2_80.resize(nLargeRJets);
      m_ljet_PassZMassLow_80.resize(nLargeRJets);
      m_ljet_PassZMassHigh_80.resize(nLargeRJets);

      m_ljet_ungrommedNtrk.resize(nLargeRJets);

      for (const auto* const ijet : event.m_largeJets) {
	std::string m_calibratedMassDecorator;
	m_calibratedMassDecorator = topConfig()->isMC()?"JetJMSScaleMomentum":"JetInsituScaleMomentum";
	static SG::AuxElement::Accessor<float> accCaloMass(m_calibratedMassDecorator+"Calo_m");
	static SG::AuxElement::Accessor<float> accCaloPt(m_calibratedMassDecorator+"Calo_pt");
	static SG::AuxElement::Accessor<float> accCaloEta(m_calibratedMassDecorator+"Calo_eta");
	static SG::AuxElement::Accessor<float> accCaloPhi(m_calibratedMassDecorator+"Calo_phi");
	static SG::AuxElement::Accessor<float> accTAMass(m_calibratedMassDecorator+"TA_m");
	static SG::AuxElement::Accessor<float> accTAPt(m_calibratedMassDecorator+"TA_pt");
	static SG::AuxElement::Accessor<float> accTAEta(m_calibratedMassDecorator+"TA_eta");
	static SG::AuxElement::Accessor<float> accTAPhi(m_calibratedMassDecorator+"TA_phi");
	m_ljet_Ntrk.push_back(ijet->getAttribute<int>("GhostTrackCount"));

	m_ljet_calomass.push_back(accCaloMass(*ijet)/1000. );
	m_ljet_calopt.push_back(accCaloPt(*ijet)/1000. );
	m_ljet_caloeta.push_back(accCaloEta(*ijet));
	m_ljet_calophi.push_back(accCaloPhi(*ijet));
	m_ljet_TAmass.push_back(accTAMass(*ijet)/1000. );
	m_ljet_TApt.push_back(accTAPt(*ijet)/1000. );
	m_ljet_TAeta.push_back(accTAEta(*ijet));
	m_ljet_TAphi.push_back(accTAPhi(*ijet));
	//ungroomed track information
	static SG::AuxElement::Accessor<ElementLink<xAOD::JetContainer> > ungroomedLink("Parent");
	const xAOD::Jet * ungroomedJet = 0;
	if( ungroomedLink.isAvailable( *ijet ) ) {
	  ElementLink<xAOD::JetContainer> linkToUngroomed = ungroomedLink( *ijet );
	  if (  linkToUngroomed.isValid() ) ungroomedJet = *linkToUngroomed;
	  std::vector<int> NTrkPt500;
	  ungroomedJet->getAttribute(xAOD::JetAttribute::NumTrkPt500, NTrkPt500);
	  if (NTrkPt500.size()==0) {
	    m_ljet_ungrommedNtrk.push_back(-99.9);
	  } else {
	    m_ljet_ungrommedNtrk.push_back(NTrkPt500.at(0));
	  }
	}

	//W/Z tagger for LCTopo trimmed jet
	unsigned long resultW_80 = ijet->auxdata<unsigned long>("TAccept_bitmap_SmoothedWZTagger_SmoothWContained80");
	unsigned long resultZ_80 = ijet->auxdata<unsigned long>("TAccept_bitmap_SmoothedWZTagger_SmoothZContained80");
	unsigned long resultW_50 = ijet->auxdata<unsigned long>("TAccept_bitmap_SmoothedWZTagger_SmoothWContained50");
	unsigned long resultZ_50 = ijet->auxdata<unsigned long>("TAccept_bitmap_SmoothedWZTagger_SmoothZContained50");
	//cut0: "ValidPtRangeHigh"    , "True if the jet is not too high pT"
	//cut1: "ValidPtRangeLow"     , "True if the jet is not too low pT"
	//cut2: "ValidEtaRange"       , "True if the jet is not too forward"
	//cut3: "ValidJetContent"     , "True if the jet is alright technicall (e.g. all attributes necessary for tag)"
	//cut4: "PassMassLow"         , "mJet > mCutLow"
	//cut5: "PassMassHigh"        , "mJet < mCutHigh"
	//cut6: "PassD2"              , "D2Jet < D2Cut"
	//cut7:  "ValidEventContent" , "True if primary vertex was found"
	//cut8: "PassNtrk"          , "NtrkJet < NtrkCut"
	//const Root::TAccept& res = m_smoothedWTagger->tag(*ijet);
	//bool isW = res.getCutResult("PassD2")&&res.getCutResult("PassMassLow")&&res.getCutResult("PassMassHigh")&&res.getCutResult("PassNtrk");
	//bool isW = ( ((result >> 9) & 1) && ((result >> 5) & 1) && ((result >> 6) & 1) && ((result >> 7) & 1) );
	//lrjIsW.push_back( isW );
	//lrjPassWNtrkD2.push_back( ((resultW>> 6) & 1) && ((resultW >> 8) & 1) );
	m_ljet_PassWNtrk_50.push_back( (resultW_50 >> 8) & 1 );
        m_ljet_PassWD2_50.push_back( (resultW_50 >> 6) & 1 );
	m_ljet_PassWMassHigh_50.push_back( (resultW_50 >> 5) & 1);
	m_ljet_PassWMassLow_50.push_back( (resultW_50 >> 4) & 1);
	//unsigned long resultZ = ijet->auxdata<unsigned long>("TAccept_bitmap_SmoothedWZTagger_SmoothWContained80");
	//lrjPassZNtrkD2.push_back( ((resultZ>> 7) & 1) && ((resultZ >> 9) & 1) );
	m_ljet_PassZNtrk_50.push_back( (resultZ_50 >> 8) & 1 );
	m_ljet_PassZD2_50.push_back( (resultZ_50 >> 6) & 1 );
	m_ljet_PassZMassHigh_50.push_back( (resultZ_50 >> 5) & 1);
	m_ljet_PassZMassLow_50.push_back( (resultZ_50 >> 4) & 1);

	m_ljet_PassZNtrk_50.push_back( (resultZ_50 >> 8) & 1 );
	m_ljet_PassZD2_50.push_back( (resultZ_50 >> 6) & 1 );
	m_ljet_PassZMassHigh_50.push_back( (resultZ_50 >> 5) & 1);
        m_ljet_PassZMassLow_50.push_back( (resultZ_50 >> 4) & 1);

	m_ljet_PassWNtrk_80.push_back( (resultW_80 >> 8) & 1 );
	m_ljet_PassWD2_80.push_back( (resultW_80 >> 6) & 1 );
	m_ljet_PassWMassHigh_80.push_back( (resultW_80 >> 5) & 1);
	m_ljet_PassWMassLow_80.push_back( (resultW_80 >> 4) & 1);
	//unsigned long resultZ = ijet->auxdata<unsigned long>("TAccept_bitmap_SmoothedWZTagger_SmoothWContained80");
	//lrjPassZNtrkD2.push_back( ((resultZ>> 7) & 1) && ((resultZ >> 9) & 1) );
	m_ljet_PassZNtrk_80.push_back( (resultZ_80 >> 8) & 1 );
	m_ljet_PassZD2_80.push_back( (resultZ_80 >> 6) & 1 );
	m_ljet_PassZMassHigh_80.push_back( (resultZ_80 >> 5) & 1);
	m_ljet_PassZMassLow_80.push_back( (resultZ_80 >> 4) & 1);

	m_hbbTag->decorate(*ijet);
	const static SG::AuxElement::ConstAccessor<float> accHbbHiggs("SubjetBScore_Higgs");
	const static SG::AuxElement::ConstAccessor<float> accHbbQCD("SubjetBScore_QCD");
	const static SG::AuxElement::ConstAccessor<float> accHbbTop("SubjetBScore_Top");
	m_ljet_HbbTop.push_back(accHbbTop(*ijet));
	m_ljet_HbbQCD.push_back(accHbbQCD(*ijet));
	m_ljet_HbbHiggs.push_back(accHbbHiggs(*ijet));

	m_hbbTagV2->decorate(*ijet);
        m_ljet_HbbTopV2.push_back(accHbbTop(*ijet));
        m_ljet_HbbQCDV2.push_back(accHbbQCD(*ijet));
        m_ljet_HbbHiggsV2.push_back(accHbbHiggs(*ijet));

	if (event.m_hashValue == topConfig()->nominalHashValue()) {
	  m_ljet_AssociatedTrackJets.resize(nLargeRJets);
	  std::set<std::size_t> m_tjMask;
	  m_tjMask.clear();
	  for (const xAOD::Jet* itj : event.m_trackJets) {
	    m_tjMask.insert(itj->index() );
	  }
	  auto tjIdxMap = constructIndexMap(m_tjMask);

	  m_ljet_AssociatedTrackJets.emplace_back();
	  auto& tjIndices = m_ljet_AssociatedTrackJets.back();
	  for (const auto& tjLink : accTrackJets(**accParent(*ijet)) ) {
	    // For now skip any invalid links. I'm not sure if this should
	    // actually trigger a job failure - input would be welcome if
	    // anyone knows.
	    if (!tjLink.isValid() ) {
	      ATH_MSG_WARNING("Invalid track link found!");
	      continue;
	    }
	    // Rely on the fact that the input containers are all shallow
	    // copies of each other so the input->output index mapping is
	    // always valid
	    auto itr = tjIdxMap.left.find(tjLink.index() );
	    if (itr != tjIdxMap.left.end() )
	      tjIndices.push_back(itr->second);
	  }
	}
      }
    }
    
    ///-- Let the base class do all the hard work --///
    top::EventSaverFlatNtuple::saveEvent(event);
  }
  
}
