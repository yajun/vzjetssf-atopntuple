/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef HOWTOEXTENDANALYSISTOP_CUSTOMEVENTSAVER_H
#define HOWTOEXTENDANALYSISTOP_CUSTOMEVENTSAVER_H

#include "TopAnalysis/EventSaverFlatNtuple.h"
// FTag includes
#include "FTagAnalysisInterfaces/IBTaggingSelectionTool.h"
#include "FlavorTagDiscriminants/HbbTag.h"
// Boost includes
#include <boost/bimap.hpp>

/**
 * This class shows you how to extend the flat ntuple to include your own variables
 * 
 * It inherits from top::EventSaverFlatNtuple, which will be doing all the hard work 
 * 
 */

namespace top{
  class CustomEventSaver : public top::EventSaverFlatNtuple {
    public:
      ///-- Default constrcutor with no arguments - needed for ROOT --///
      CustomEventSaver();
      ///-- Destructor does nothing --///
      virtual ~CustomEventSaver(){}
      
      ///-- initialize function for top::EventSaverFlatNtuple --///
      ///-- We will be setting up out custom variables here --///
      virtual void initialize(std::shared_ptr<top::TopConfig> config, TFile* file, const std::vector<std::string>& extraBranches) override;
      
      ///-- Keep the asg::AsgTool happy --///
      virtual StatusCode initialize() override {return StatusCode::SUCCESS;}      
      
      ///-- saveEvent function for top::EventSaverFlatNtuple --///
      ///-- We will be setting our custom variables on a per-event basis --///
      virtual void saveEvent(const top::Event& event) override;
      
    private:
      ///-- Some additional custom variables for the output --///
      float m_randomNumber;
      float m_someOtherVariable;

      //The hbbTagTool
      std::unique_ptr<FlavorTagDiscriminants::HbbTag> m_hbbTag;
      std::unique_ptr<FlavorTagDiscriminants::HbbTag> m_hbbTagV2;
      //additional custom variables for muons
      std::vector<unsigned short> m_mu_type; //nominal only
      std::vector<unsigned char> m_mu_quality; // nominal only
      std::vector<float> m_mu_eloss; 

      std::vector<float> m_tjet_HadronLabel;

      //additional custom variables for large-R jets
      std::vector<float> m_ljet_calomass;
      std::vector<float> m_ljet_calopt;
      std::vector<float> m_ljet_caloeta;
      std::vector<float> m_ljet_calophi;

      std::vector<float> m_ljet_TAmass;
      std::vector<float> m_ljet_TApt;
      std::vector<float> m_ljet_TAeta;
      std::vector<float> m_ljet_TAphi;

      std::vector<float> m_ljet_truthmass;
      std::vector<float> m_ljet_truthpt;
      std::vector<float> m_ljet_trutheta;
      std::vector<float> m_ljet_truthphi;

      std::vector<float> m_ljet_HbbTop;
      std::vector<float> m_ljet_HbbQCD;
      std::vector<float> m_ljet_HbbHiggs;
      std::vector<float> m_ljet_HbbTopV2;
      std::vector<float> m_ljet_HbbQCDV2;
      std::vector<float> m_ljet_HbbHiggsV2;
      std::vector<int> m_ljet_Ntrk;
      std::vector<int> m_ljet_PassWNtrk_50;
      std::vector<int> m_ljet_PassWD2_50;
      std::vector<int> m_ljet_PassWMassLow_50;
      std::vector<int> m_ljet_PassWMassHigh_50;

      std::vector<int> m_ljet_PassWNtrk_80;
      std::vector<int> m_ljet_PassWD2_80;
      std::vector<int> m_ljet_PassWMassLow_80;
      std::vector<int> m_ljet_PassWMassHigh_80;

      std::vector<int> m_ljet_PassZNtrk_50;
      std::vector<int> m_ljet_PassZD2_50;
      std::vector<int> m_ljet_PassZMassLow_50;
      std::vector<int> m_ljet_PassZMassHigh_50;

      std::vector<int> m_ljet_PassZNtrk_80;
      std::vector<int> m_ljet_PassZD2_80;
      std::vector<int> m_ljet_PassZMassLow_80;
      std::vector<int> m_ljet_PassZMassHigh_80;

      std::vector<float> m_ljet_ungrommedNtrk;
      std::vector<std::vector<std::size_t>> m_ljet_AssociatedTrackJets;

      ///-- Tell RootCore to build a dictionary (we need this) --///
      ClassDefOverride(top::CustomEventSaver, 0);
  };
}

#endif
